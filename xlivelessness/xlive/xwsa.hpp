#pragma once

extern CRITICAL_SECTION xlive_critsec_overlapped_sockets;

void XWSADeleteOverlappedSocket_(SOCKET perpetual_socket, SOCKET transitory_socket);
