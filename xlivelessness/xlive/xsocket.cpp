#include <winsock2.h>
#include "xdefs.hpp"
#include "xsocket.hpp"
#include "xlive.hpp"
#include "../xlln/xlln.hpp"
#include "../xlln/debug-log.hpp"
#include "../xlln/wnd-sockets.hpp"
#include "../xlln/xlln-keep-alive.hpp"
#include "xnet.hpp"
#include "xwsa.hpp"
#include "xlocator.hpp"
#include "xnetqos.hpp"
#include "packet-handler.hpp"
#include "net-entity.hpp"
#include "../utils/utils.hpp"
#include "../utils/util-socket.hpp"
#include <map>

uint16_t xlive_base_port = 0;
uint16_t xlive_base_port_broadcast_spacing_start = 2000;
uint16_t xlive_base_port_broadcast_spacing_increment = 100;
uint16_t xlive_base_port_broadcast_spacing_end = 2400;
HANDLE xlive_base_port_mutex = 0;
uint16_t xlive_port_online = 3074;
uint16_t xlive_port_system_link = 0;
bool xlive_netsocket_abort = false;

CRITICAL_SECTION xlive_critsec_sockets;
// Key: transitorySocket.
std::map<SOCKET, SOCKET_MAPPING_INFO*> xlive_socket_info;
// Value: transitorySocket.
static std::map<uint16_t, SOCKET> xlive_port_offset_sockets;
// Key: perpetualSocket. Value: transitorySocket.
std::map<SOCKET, SOCKET> xlive_xsocket_perpetual_to_transitory_socket;

SOCKET xlive_xsocket_perpetual_core_socket = INVALID_SOCKET;

static void XLLNCreateCoreSocket();

// Get transitorySocket (real socket) from perpetualSocket (fake handle).
SOCKET XSocketGetTransitorySocket_(SOCKET perpetual_socket)
{
	SOCKET transitorySocket = INVALID_SOCKET;
	
	if (xlive_xsocket_perpetual_to_transitory_socket.count(perpetual_socket)) {
		transitorySocket = xlive_xsocket_perpetual_to_transitory_socket[perpetual_socket];
	}
	
	if (transitorySocket == INVALID_SOCKET) {
		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
			, "%s unknown perpetual_socket (0x%zx)."
			, __func__
			, perpetual_socket
		);
	}
	
	return transitorySocket;
}
// Get transitorySocket (real socket) from perpetualSocket (fake handle).
SOCKET XSocketGetTransitorySocket(SOCKET perpetual_socket, bool* connectionless_socket)
{
	SOCKET transitorySocket = INVALID_SOCKET;
	
	{
		EnterCriticalSection(&xlive_critsec_sockets);
		
		if (xlive_xsocket_perpetual_to_transitory_socket.count(perpetual_socket)) {
			transitorySocket = xlive_xsocket_perpetual_to_transitory_socket[perpetual_socket];
			SOCKET_MAPPING_INFO* socketMappingInfo = xlive_socket_info[transitorySocket];
			
			if (connectionless_socket) {
				*connectionless_socket = socketMappingInfo->protocol == IPPROTO_UDP;
			}
			
			// If the port has not been explicitly binded. Check if it was implicitly and update it.
			if (!socketMappingInfo->shutdown && socketMappingInfo->portBindHBO == 0) {
				SOCKADDR_STORAGE sockAddrExternal;
				int32_t sockAddrExternalSize = (int32_t)sizeof(sockAddrExternal);
				int32_t resultGetsockname = getsockname(transitorySocket, (sockaddr*)&sockAddrExternal, &sockAddrExternalSize);
				int32_t errorGetsockname = WSAGetLastError();
				if (!resultGetsockname) {
					socketMappingInfo->portBindHBO = GetSockAddrPort(&sockAddrExternal);
					
					XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_INFO
						, "%s socket (P,T) (0x%zx,0x%zx) was implicitly binded to %hu."
						, __func__
						, perpetual_socket
						, transitorySocket
						, socketMappingInfo->portBindHBO
					);
					
					XllnWndSocketsInvalidateSockets();
				}
			}
		}
		
		LeaveCriticalSection(&xlive_critsec_sockets);
	}
	
	if (transitorySocket == INVALID_SOCKET) {
		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
			, "%s unknown perpetual_socket (0x%zx)."
			, __func__
			, perpetual_socket
		);
	}
	
	return transitorySocket;
}
SOCKET XSocketGetTransitorySocket(SOCKET perpetual_socket)
{
	return XSocketGetTransitorySocket(perpetual_socket, 0);
}

bool XSocketPerpetualSocketChangedError_(int32_t errorSocket, SOCKET perpetual_socket, SOCKET transitory_socket)
{
	bool result = false;
	
	if (
		(errorSocket == WSAEINTR || errorSocket == WSAENOTSOCK)
		&& xlive_xsocket_perpetual_to_transitory_socket.count(perpetual_socket)
		&& xlive_xsocket_perpetual_to_transitory_socket[perpetual_socket] != transitory_socket
	) {
		result = true;
	}
	
	return result;
}

bool XSocketPerpetualSocketChangedError(int32_t errorSocket, SOCKET perpetual_socket, SOCKET transitory_socket)
{
	bool result = false;
	
	{
		EnterCriticalSection(&xlive_critsec_sockets);
		
		result = XSocketPerpetualSocketChangedError_(errorSocket, perpetual_socket, transitory_socket);
		
		LeaveCriticalSection(&xlive_critsec_sockets);
	}
	
	return result;
}

uint8_t XSocketPerpetualSocketChanged_(SOCKET perpetual_socket, SOCKET transitory_socket)
{
	uint8_t result = 0;
	
	if (!xlive_xsocket_perpetual_to_transitory_socket.count(perpetual_socket)) {
		result = 1;
	}
	else if (xlive_xsocket_perpetual_to_transitory_socket[perpetual_socket] != transitory_socket) {
		result = 2;
	}
	
	return result;
}

// If perpetual_sockets is not null and perpetual_sockets_count > 0 then only rebind the perpetual sockets provided in the array.
// Otherwise rebind all possible sockets.
void XSocketRebindSockets_(SOCKET* perpetual_sockets, uint32_t perpetual_sockets_count)
{
	std::map<SOCKET_MAPPING_INFO*, SOCKET_MAPPING_INFO*> oldToNew;
	
	for (const auto &socketInfo : xlive_socket_info) {
		SOCKET_MAPPING_INFO &socketMappingInfoOld = *socketInfo.second;
		if (socketMappingInfoOld.protocol != IPPROTO_UDP) {
			// TODO support more than just UDP (like TCP).
			XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_WARN
				, "%s transitorySocket (0x%zx) protocol (%d) unsupported for rebinding."
				, __func__
				, socketMappingInfoOld.transitorySocket
				, socketMappingInfoOld.protocol
			);
			continue;
		}
		
		if (socketMappingInfoOld.portBindHBO == 0) {
			// The port was never binded so doesn't have to be re-binded.
			continue;
		}
		
		if (perpetual_sockets && perpetual_sockets_count > 0) {
			bool foundSocket = false;
			for (uint32_t iPerpetualSockets = 0; iPerpetualSockets < perpetual_sockets_count; iPerpetualSockets++) {
				if (perpetual_sockets[iPerpetualSockets] == socketMappingInfoOld.perpetualSocket) {
					foundSocket = true;
					break;
				}
			}
			if (!foundSocket) {
				continue;
			}
		}
		
		SOCKET_MAPPING_INFO* socketMappingInfoNew = (oldToNew[&socketMappingInfoOld] = new SOCKET_MAPPING_INFO);
		*socketMappingInfoNew = socketMappingInfoOld;
		socketMappingInfoNew->transitorySocket = socket(AF_INET, socketMappingInfoNew->type, socketMappingInfoNew->protocol);
		if (socketMappingInfoNew->transitorySocket == INVALID_SOCKET) {
			int32_t errorSocket = WSAGetLastError();
			XLLN_DEBUG_LOG_ECODE(errorSocket, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
				, "%s create socket error on perpetualSocket (0x%zx)."
				, __func__
				, socketMappingInfoOld.perpetualSocket
			);
		}
		
		// If it was a random port, make sure when we rebind that it will be another random port since portBindHBO will be the port we will attempt to remake.
		if (socketMappingInfoNew->portOgHBO == 0) {
			socketMappingInfoNew->portBindHBO = 0;
		}
	}
	
	for (const auto &socketInfos : oldToNew) {
		SOCKET_MAPPING_INFO &socketMappingInfoOld = *socketInfos.first;
		SOCKET_MAPPING_INFO &socketMappingInfoNew = *socketInfos.second;
		
		xlive_socket_info[socketMappingInfoNew.transitorySocket] = &socketMappingInfoNew;
		xlive_xsocket_perpetual_to_transitory_socket[socketMappingInfoNew.perpetualSocket] = socketMappingInfoNew.transitorySocket;
		// Update the Core socket mapping too if this is its transitory socket.
		if (xlive_xsocket_perpetual_to_transitory_socket.count(xlive_xsocket_perpetual_core_socket) && xlive_xsocket_perpetual_to_transitory_socket[xlive_xsocket_perpetual_core_socket] == socketMappingInfoOld.transitorySocket) {
			xlive_xsocket_perpetual_to_transitory_socket[xlive_xsocket_perpetual_core_socket] = socketMappingInfoNew.transitorySocket;
		}
		xlive_socket_info.erase(socketMappingInfoOld.transitorySocket);
		
		int32_t resultShutdown = shutdown(socketMappingInfoOld.transitorySocket, SD_RECEIVE);
		if (resultShutdown) {
			int32_t errorShutdown = WSAGetLastError();
			XLLN_DEBUG_LOG_ECODE(errorShutdown, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
				, "%s shutdown error on transitorySocket (0x%zx)."
				, __func__
				, socketMappingInfoOld.transitorySocket
			);
		}
		
		int32_t resultCloseSocket = closesocket(socketMappingInfoOld.transitorySocket);
		if (resultCloseSocket) {
			int32_t errorCloseSocket = WSAGetLastError();
			XLLN_DEBUG_LOG_ECODE(errorCloseSocket, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
				, "%s close socket error on transitorySocket (0x%zx)."
				, __func__
				, socketMappingInfoOld.transitorySocket
			);
		}
	}
	
	for (const auto &socketInfos : oldToNew) {
		SOCKET_MAPPING_INFO &socketMappingInfoOld = *socketInfos.first;
		SOCKET_MAPPING_INFO &socketMappingInfoNew = *socketInfos.second;
		
		for (const auto &optionNameValue : socketMappingInfoNew.socketOptions) {
			int32_t resultSetSockOpt = setsockopt(socketMappingInfoNew.transitorySocket, socketMappingInfoNew.protocol == IPPROTO_TCP ? IPPROTO_TCP : SOL_SOCKET, optionNameValue.first, (const char*)&optionNameValue.second, sizeof(uint32_t));
			if (resultSetSockOpt) {
				int32_t errorSetSockOpt = WSAGetLastError();
				XLLN_DEBUG_LOG_ECODE(errorSetSockOpt, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
					, "%s setsockopt failed to set option (0x%08x, 0x%08x) on new transitorySocket (0x%zx)."
					, __func__
					, optionNameValue.first
					, optionNameValue.second
					, socketMappingInfoNew.transitorySocket
				);
			}
		}
		
		for (const auto &optionNameValue : socketMappingInfoNew.socketIoctl) {
			u_long cmdValue = optionNameValue.second;
			int32_t resultIoctlSocket = ioctlsocket(socketMappingInfoNew.transitorySocket, optionNameValue.first, &cmdValue);
			if (resultIoctlSocket) {
				int32_t errorIoctlSocket = WSAGetLastError();
				XLLN_DEBUG_LOG_ECODE(errorIoctlSocket, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
					, "%s ioctlsocket failed to set command (0x%08x, 0x%08x) on new transitorySocket (0x%zx)."
					, __func__
					, optionNameValue.first
					, optionNameValue.second
					, socketMappingInfoNew.transitorySocket
				);
			}
		}
		
		sockaddr_in socketBindAddress;
		socketBindAddress.sin_family = AF_INET;
		socketBindAddress.sin_addr.s_addr = htonl(INADDR_ANY);
		socketBindAddress.sin_port = htons(socketMappingInfoNew.portBindHBO);
		{
			EnterCriticalSection(&xlive_critsec_network_adapter);
			// TODO Should we perhaps not do this unless the user really wants to like if (xlive_init_preferred_network_adapter_name or config) is set?
			if (xlive_network_adapter && xlive_network_adapter->unicastHAddr != INADDR_LOOPBACK) {
				socketBindAddress.sin_addr.s_addr = htonl(xlive_network_adapter->unicastHAddr);
			}
			LeaveCriticalSection(&xlive_critsec_network_adapter);
		}
		SOCKET resultSocketBind = bind(socketMappingInfoNew.transitorySocket, (sockaddr*)&socketBindAddress, sizeof(socketBindAddress));
		if (resultSocketBind) {
			int32_t errorBind = WSAGetLastError();
			XLLN_DEBUG_LOG_ECODE(errorBind, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
				, "%s bind error on new transitorySocket (0x%zx)."
				, __func__
				, socketMappingInfoNew.transitorySocket
			);
		}
		
		delete &socketMappingInfoOld;
	}
	
	XllnWndSocketsInvalidateSockets();
}
void XSocketRebindAllSockets()
{
	EnterCriticalSection(&xlive_critsec_sockets);
	
	XSocketRebindSockets_(0, 0);
	
	LeaveCriticalSection(&xlive_critsec_sockets);
}

// #3
SOCKET WINAPI XSocketCreate(int32_t address_family, int32_t type, int32_t protocol)
{
	TRACE_FX();
	if (address_family != AF_INET) {
		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
			, "%s address_family (%d) must be AF_INET (%d)."
			, __func__
			, address_family
			, AF_INET
		);
		WSASetLastError(WSAEAFNOSUPPORT);
		return INVALID_SOCKET;
	}
	if (type == 0 && protocol == 0) {
		type = SOCK_STREAM;
		protocol = IPPROTO_TCP;
	}
	else if (type == SOCK_DGRAM && protocol == 0) {
		protocol = IPPROTO_UDP;
	}
	else if (type == SOCK_STREAM && protocol == 0) {
		protocol = IPPROTO_TCP;
	}
	bool isVdp = false;
	if (protocol == IPPROTO_VDP) {
		isVdp = true;
		// We can't support VDP (Voice / Data Protocol) it's some encrypted crap which isn't standard.
		protocol = IPPROTO_UDP;
	}
	if (type != SOCK_STREAM && type != SOCK_DGRAM) {
		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
			, "%s type (%d) must be either SOCK_STREAM (%d) or SOCK_DGRAM (%d)."
			, __func__
			, type
			, SOCK_STREAM
			, SOCK_DGRAM
		);
		WSASetLastError(WSAEOPNOTSUPP);
		return INVALID_SOCKET;
	}
	
	SOCKET transitorySocket = socket(address_family, type, protocol);
	int32_t errorSocketCreate = WSAGetLastError();
	
	if (transitorySocket == INVALID_SOCKET) {
		XLLN_DEBUG_LOG_ECODE(errorSocketCreate, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
			, "%s create socket failed address_family (%d) type (%d) protocol (%d)."
			, __func__
			, address_family
			, type
			, protocol
		);
		
		WSASetLastError(errorSocketCreate);
		return INVALID_SOCKET;
	}
	
	SOCKET perpetualSocket = (SOCKET)CreateMutexA(NULL, false, NULL);
	
	XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_INFO
		, "%s socket (P,T) (0x%zx,0x%zx) address_family (%d) type (%d) protocol (%d).%s"
		, __func__
		, perpetualSocket
		, transitorySocket
		, address_family
		, type
		, protocol
		, isVdp ? " Is a VDP Socket." : ""
	);
	
	SOCKET_MAPPING_INFO* socketMappingInfo = new SOCKET_MAPPING_INFO;
	socketMappingInfo->transitorySocket = transitorySocket;
	socketMappingInfo->perpetualSocket = perpetualSocket;
	socketMappingInfo->type = type;
	socketMappingInfo->protocol = protocol;
	socketMappingInfo->isVdpProtocol = isVdp;
	
	{
		EnterCriticalSection(&xlive_critsec_sockets);
		
		xlive_socket_info[transitorySocket] = socketMappingInfo;
		xlive_xsocket_perpetual_to_transitory_socket[perpetualSocket] = transitorySocket;
		
		XllnWndSocketsInvalidateSockets();
		
		LeaveCriticalSection(&xlive_critsec_sockets);
	}
	
	WSASetLastError(ERROR_SUCCESS);
	return perpetualSocket;
}

// #4
int32_t WINAPI XSocketClose(SOCKET perpetual_socket)
{
	TRACE_FX();
	
	XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_INFO
		, "%s perpetual_socket (0x%zx)."
		, __func__
		, perpetual_socket
	);
	
	while (1) {
		SOCKET transitorySocket = XSocketGetTransitorySocket(perpetual_socket);
		if (transitorySocket == INVALID_SOCKET) {
			WSASetLastError(WSAENOTSOCK);
			return SOCKET_ERROR;
		}
		
		int32_t resultCloseSocket = closesocket(transitorySocket);
		int32_t errorCloseSocket = WSAGetLastError();
		
		{
			EnterCriticalSection(&xlive_critsec_sockets);
			
			if (resultCloseSocket) {
				if (XSocketPerpetualSocketChangedError_(errorCloseSocket, perpetual_socket, transitorySocket)) {
					LeaveCriticalSection(&xlive_critsec_sockets);
					continue;
				}
				LeaveCriticalSection(&xlive_critsec_sockets);
				
				XLLN_DEBUG_LOG_ECODE(errorCloseSocket, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
					, "%s close socket error on socket (P,T) (0x%zx,0x%zx)."
					, __func__
					, perpetual_socket
					, transitorySocket
				);
				
				WSASetLastError(errorCloseSocket);
				return SOCKET_ERROR;
			}
			
			uint8_t resultPerpetualChanged = XSocketPerpetualSocketChanged_(perpetual_socket, transitorySocket);
			if (resultPerpetualChanged) {
				LeaveCriticalSection(&xlive_critsec_sockets);
				if (resultPerpetualChanged == 2) {
					continue;
				}
				WSASetLastError(WSAENOTSOCK);
				return SOCKET_ERROR;
			}
			
			XWSADeleteOverlappedSocket_(perpetual_socket, transitorySocket);
			
			SOCKET_MAPPING_INFO* socketMappingInfo = xlive_socket_info[transitorySocket];
			xlive_socket_info.erase(transitorySocket);
			if (socketMappingInfo->portOffsetHBO != (int16_t)-1) {
				xlive_port_offset_sockets.erase(socketMappingInfo->portOffsetHBO);
			}
			
			bool createCoreSocket = false;
			if (xlive_xsocket_perpetual_core_socket != INVALID_SOCKET && perpetual_socket != xlive_xsocket_perpetual_core_socket && transitorySocket == xlive_xsocket_perpetual_to_transitory_socket[xlive_xsocket_perpetual_core_socket]) {
				createCoreSocket = true;
				xlive_xsocket_perpetual_to_transitory_socket.erase(xlive_xsocket_perpetual_core_socket);
				CloseHandle((HANDLE)xlive_xsocket_perpetual_core_socket);
				xlive_xsocket_perpetual_core_socket = INVALID_SOCKET;
			}
			
			xlive_xsocket_perpetual_to_transitory_socket.erase(perpetual_socket);
			CloseHandle((HANDLE)perpetual_socket);
			
			XllnWndSocketsInvalidateSockets();
			
			LeaveCriticalSection(&xlive_critsec_sockets);
			
			socketMappingInfo->socketOptions.clear();
			socketMappingInfo->socketIoctl.clear();
			
			delete socketMappingInfo;
			socketMappingInfo = 0;
			
			if (createCoreSocket) {
				XLLNCreateCoreSocket();
			}
		}
		
		WSASetLastError(errorCloseSocket);
		return resultCloseSocket;
	}
}

// #5
int32_t WINAPI XSocketShutdown(SOCKET perpetual_socket, int32_t how)
{
	TRACE_FX();
	
	XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_INFO
		, "%s perpetual_socket (0x%zx) how (%d)."
		, __func__
		, perpetual_socket
		, how
	);
	
	while (1) {
		SOCKET transitorySocket = XSocketGetTransitorySocket(perpetual_socket);
		if (transitorySocket == INVALID_SOCKET) {
			WSASetLastError(WSAENOTSOCK);
			return SOCKET_ERROR;
		}
		
		int32_t resultShutdown = shutdown(transitorySocket, how);
		int32_t errorShutdown = WSAGetLastError();
		
		{
			EnterCriticalSection(&xlive_critsec_sockets);
			
			if (resultShutdown) {
				if (XSocketPerpetualSocketChangedError_(errorShutdown, perpetual_socket, transitorySocket)) {
					LeaveCriticalSection(&xlive_critsec_sockets);
					continue;
				}
				LeaveCriticalSection(&xlive_critsec_sockets);
				
				XLLN_DEBUG_LOG_ECODE(errorShutdown, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
					, "%s shutdown socket error on socket (P,T) (0x%zx,0x%zx) how (%d)."
					, __func__
					, perpetual_socket
					, transitorySocket
					, how
				);
				
				WSASetLastError(errorShutdown);
				return SOCKET_ERROR;
			}
			
			uint8_t resultPerpetualChanged = XSocketPerpetualSocketChanged_(perpetual_socket, transitorySocket);
			if (resultPerpetualChanged) {
				LeaveCriticalSection(&xlive_critsec_sockets);
				if (resultPerpetualChanged == 2) {
					continue;
				}
				WSASetLastError(WSAENOTSOCK);
				return SOCKET_ERROR;
			}
			
			SOCKET_MAPPING_INFO* socketMappingInfo = xlive_socket_info[transitorySocket];
			if (socketMappingInfo->portOffsetHBO != (int16_t)-1) {
				xlive_port_offset_sockets.erase(socketMappingInfo->portOffsetHBO);
			}
			socketMappingInfo->shutdown = true;
			socketMappingInfo->portBindHBO = 0;
			
			XllnWndSocketsInvalidateSockets();
			
			LeaveCriticalSection(&xlive_critsec_sockets);
		}
		
		WSASetLastError(errorShutdown);
		return resultShutdown;
	}
}

// #6
int32_t WINAPI XSocketIOCTLSocket(SOCKET perpetual_socket, int32_t command, unsigned long* command_value)
{
	TRACE_FX();
	
	while (1) {
		SOCKET transitorySocket = XSocketGetTransitorySocket(perpetual_socket);
		if (transitorySocket == INVALID_SOCKET) {
			WSASetLastError(WSAENOTSOCK);
			return SOCKET_ERROR;
		}
		
		int32_t resultIoctlSocket = ioctlsocket(transitorySocket, command, command_value);
		int32_t errorIoctlSocket = WSAGetLastError();
		
		bool ignoreLogging = false;
		
		{
			EnterCriticalSection(&xlive_critsec_sockets);
			
			if (resultIoctlSocket) {
				if (XSocketPerpetualSocketChangedError_(errorIoctlSocket, perpetual_socket, transitorySocket)) {
					LeaveCriticalSection(&xlive_critsec_sockets);
					continue;
				}
				LeaveCriticalSection(&xlive_critsec_sockets);
				
				XLLN_DEBUG_LOG_ECODE(errorIoctlSocket, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
					, "%s ioctlsocket error on socket (P,T) (0x%zx,0x%zx)."
					, __func__
					, perpetual_socket
					, transitorySocket
				);
				
				WSASetLastError(errorIoctlSocket);
				return SOCKET_ERROR;
			}
			
			uint8_t resultPerpetualChanged = XSocketPerpetualSocketChanged_(perpetual_socket, transitorySocket);
			if (resultPerpetualChanged) {
				LeaveCriticalSection(&xlive_critsec_sockets);
				if (resultPerpetualChanged == 2) {
					continue;
				}
				WSASetLastError(WSAENOTSOCK);
				return SOCKET_ERROR;
			}
			
			SOCKET_MAPPING_INFO* socketMappingInfo = xlive_socket_info[transitorySocket];
			
			switch (command) {
				// Ignore read commands.
				case FIONREAD:
				case SIOCGHIWAT:
				case SIOCGLOWAT:
				case SIOCATMARK:
					ignoreLogging = true;
					break;
				default: {
					socketMappingInfo->socketIoctl[command] = *command_value;
					
					XllnWndSocketsInvalidateSockets();
					break;
				}
			}
			
			LeaveCriticalSection(&xlive_critsec_sockets);
		}
		
		if (!ignoreLogging) {
			XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_DEBUG
				, "%s on socket (P,T) (0x%zx,0x%zx): command (0x%08x) value (0x%08x)."
				, __func__
				, perpetual_socket
				, transitorySocket
				, command
				, *command_value
			);
		}
		
		WSASetLastError(errorIoctlSocket);
		return resultIoctlSocket;
	}
}

// #7
int32_t WINAPI XSocketSetSockOpt(SOCKET perpetual_socket, int32_t option_level, int32_t option_name, const char* option_value, int32_t option_size)
{
	TRACE_FX();
	
	while (1) {
		SOCKET transitorySocket = XSocketGetTransitorySocket(perpetual_socket);
		if (transitorySocket == INVALID_SOCKET) {
			WSASetLastError(WSAENOTSOCK);
			return SOCKET_ERROR;
		}
		
		int32_t resultSetSockOpt = setsockopt(transitorySocket, option_level, option_name, option_value, option_size);
		int32_t errorSetSockOpt = WSAGetLastError();
		
		{
			EnterCriticalSection(&xlive_critsec_sockets);
			
			if (resultSetSockOpt) {
				if (XSocketPerpetualSocketChangedError_(errorSetSockOpt, perpetual_socket, transitorySocket)) {
					LeaveCriticalSection(&xlive_critsec_sockets);
					continue;
				}
				LeaveCriticalSection(&xlive_critsec_sockets);
				
				XLLN_DEBUG_LOG_ECODE(errorSetSockOpt, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
					, "%s setsockopt on socket (P,T) (0x%zx,0x%zx)."
					, __func__
					, perpetual_socket
					, transitorySocket
				);
				
				WSASetLastError(errorSetSockOpt);
				return SOCKET_ERROR;
			}
			
			uint8_t resultPerpetualChanged = XSocketPerpetualSocketChanged_(perpetual_socket, transitorySocket);
			if (resultPerpetualChanged) {
				LeaveCriticalSection(&xlive_critsec_sockets);
				if (resultPerpetualChanged == 2) {
					continue;
				}
				WSASetLastError(WSAENOTSOCK);
				return SOCKET_ERROR;
			}
			
			SOCKET_MAPPING_INFO* socketMappingInfo = xlive_socket_info[transitorySocket];
			
			uint32_t optionValue = 0;
			memcpy(&optionValue, option_value, option_size > 4 ? 4 : option_size);
			socketMappingInfo->socketOptions[option_name] = optionValue;
			
			if (option_level == SOL_SOCKET && option_name == SO_BROADCAST) {
				socketMappingInfo->broadcast = optionValue > 0;
			}
			
			XllnWndSocketsInvalidateSockets();
			
			LeaveCriticalSection(&xlive_critsec_sockets);
			
			XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_DEBUG
				, "%s on socket (P,T) (0x%zx,0x%zx): option_level (0x%08x) option_name (0x%08x) option_value (0x%08x)."
				, __func__
				, perpetual_socket
				, transitorySocket
				, option_level
				, option_name
				, optionValue
			);
		}
		
		WSASetLastError(errorSetSockOpt);
		return resultSetSockOpt;
	}
}

// #8
int32_t WINAPI XSocketGetSockOpt(SOCKET perpetual_socket, int32_t option_level, int32_t option_name, char* option_value, int32_t* option_size)
{
	TRACE_FX();
	
	while (1) {
		SOCKET transitorySocket = XSocketGetTransitorySocket(perpetual_socket);
		if (transitorySocket == INVALID_SOCKET) {
			WSASetLastError(WSAENOTSOCK);
			return SOCKET_ERROR;
		}
		
		int32_t resultGetsockopt = getsockopt(transitorySocket, option_level, option_name, option_value, option_size);
		int32_t errorGetsockopt = WSAGetLastError();
		
		if (resultGetsockopt && XSocketPerpetualSocketChangedError(errorGetsockopt, perpetual_socket, transitorySocket)) {
			continue;
		}
		
		if (resultGetsockopt) {
			XLLN_DEBUG_LOG_ECODE(errorGetsockopt, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
				, "%s getsockopt error on socket (P,T) (0x%zx,0x%zx)."
				, __func__
				, perpetual_socket
				, transitorySocket
			);
		}
		else {
			uint32_t optionValue = 0;
			memcpy(&optionValue, option_value, *option_size > 4 ? 4 : *option_size);
			XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_DEBUG
				, "%s on socket (P,T) (0x%zx,0x%zx): option_level (0x%08x) option_name (0x%08x) option_value (0x%08x)."
				, __func__
				, perpetual_socket
				, transitorySocket
				, option_level
				, option_name
				, optionValue
			);
		}
		
		WSASetLastError(errorGetsockopt);
		return resultGetsockopt;
	}
}

// #9
int32_t WINAPI XSocketGetSockName(SOCKET perpetual_socket, sockaddr* name, int32_t* name_size)
{
	TRACE_FX();
	
	while (1) {
		SOCKET transitorySocket = XSocketGetTransitorySocket(perpetual_socket);
		if (transitorySocket == INVALID_SOCKET) {
			WSASetLastError(WSAENOTSOCK);
			return SOCKET_ERROR;
		}
		
		int32_t resultGetsockname = getsockname(transitorySocket, name, name_size);
		int32_t errorGetsockname = WSAGetLastError();
		
		if (resultGetsockname && XSocketPerpetualSocketChangedError(errorGetsockname, perpetual_socket, transitorySocket)) {
			continue;
		}
		
		if (resultGetsockname) {
			XLLN_DEBUG_LOG_ECODE(errorGetsockname, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
				, "%s getsockname error on socket (P,T) (0x%zx,0x%zx)."
				, __func__
				, perpetual_socket
				, transitorySocket
			);
		}
		
		WSASetLastError(errorGetsockname);
		return resultGetsockname;
	}
}

// #10
int32_t WINAPI XSocketGetPeerName(SOCKET perpetual_socket, sockaddr* sock_addr_xlive, int32_t* sock_addr_xlive_size)
{
	TRACE_FX();
	if (!sock_addr_xlive) {
		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
			, "%s sock_addr_xlive is NULL."
			, __func__
		);
		WSASetLastError(WSAEFAULT);
		return SOCKET_ERROR;
	}
	if (!sock_addr_xlive_size) {
		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
			, "%s sock_addr_xlive_size is NULL."
			, __func__
		);
		WSASetLastError(WSAEFAULT);
		return SOCKET_ERROR;
	}
	if (*sock_addr_xlive_size < sizeof(sockaddr_in)) {
		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
			, "%s *sock_addr_xlive_size (0x%08x) < sizeof(sockaddr_in) (0x%zx)."
			, __func__
			, *sock_addr_xlive_size
			, (size_t)sizeof(sockaddr_in)
		);
		WSASetLastError(WSAEINVAL);
		return SOCKET_ERROR;
	}
	
	uint32_t tcpRemoteInstanceId = 0;
	uint16_t tcpRemotePortHBO = 0;
	SOCKET transitorySocket = INVALID_SOCKET;
	
	{
		EnterCriticalSection(&xlive_critsec_sockets);
		
		const auto &itrTransitorySocket = xlive_xsocket_perpetual_to_transitory_socket.find(perpetual_socket);
		if (itrTransitorySocket != xlive_xsocket_perpetual_to_transitory_socket.end()) {
			transitorySocket = itrTransitorySocket->second;
			SOCKET_MAPPING_INFO* socketMappingInfo = xlive_socket_info[transitorySocket];
			
			tcpRemoteInstanceId = socketMappingInfo->tcpRemoteInstanceId;
			tcpRemotePortHBO = socketMappingInfo->tcpRemotePortHBO;
		}
		
		LeaveCriticalSection(&xlive_critsec_sockets);
	}
	
	if (transitorySocket == INVALID_SOCKET) {
		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
			, "%s unknown perpetual_socket (0x%zx)."
			, __func__
			, perpetual_socket
		);
		WSASetLastError(WSAENOTSOCK);
		return SOCKET_ERROR;
	}
	
	if (!tcpRemoteInstanceId) {
		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
			, "%s socket (P,T) (0x%zx,0x%zx) is not connected to a peer."
			, __func__
			, perpetual_socket
			, transitorySocket
		);
		WSASetLastError(WSAENOTCONN);
		return SOCKET_ERROR;
	}
	
	XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_DEBUG
		, "%s socket (P,T) (0x%zx,0x%zx) is connected to NetEntity 0x%08x:%hu."
		, __func__
		, perpetual_socket
		, transitorySocket
		, tcpRemoteInstanceId
		, tcpRemotePortHBO
	);
	
	sockaddr_in* sockAddrIpv4Xlive = ((sockaddr_in*)sock_addr_xlive);
	sockAddrIpv4Xlive->sin_family = AF_INET;
	sockAddrIpv4Xlive->sin_addr.s_addr = htonl(tcpRemoteInstanceId);
	sockAddrIpv4Xlive->sin_port = htons(tcpRemotePortHBO);
	*sock_addr_xlive_size = sizeof(sockaddr_in);
		
	WSASetLastError(ERROR_SUCCESS);
	return 0;
}

// #11
SOCKET WINAPI XSocketBind(SOCKET perpetual_socket, const sockaddr* name, int32_t name_size)
{
	TRACE_FX();
	
	SOCKADDR_STORAGE sockAddrExternal;
	int32_t sockAddrExternalSize = (int32_t)sizeof(sockAddrExternal);
	memcpy(&sockAddrExternal, name, sockAddrExternalSize < name_size ? sockAddrExternalSize : name_size);
	
	if (sockAddrExternal.ss_family != AF_INET && sockAddrExternal.ss_family != AF_INET6) {
		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
			, "%s perpetual_socket (0x%zx) bind on unsupported socket address family 0x%04hx."
			, __func__
			, perpetual_socket
			, sockAddrExternal.ss_family
		);
		
		WSASetLastError(WSAEAFNOSUPPORT);
		return SOCKET_ERROR;
	}
	
	uint16_t portHBO = GetSockAddrPort(&sockAddrExternal);
	uint16_t portOffset = portHBO % 100;
	const uint16_t portBase = portHBO - portOffset;
	uint16_t portShiftedHBO = 0;
	
	// If not a random port (0), and we are using base ports, then shift the port.
	if (portHBO && IsUsingBasePort(xlive_base_port)) {
		{
			EnterCriticalSection(&xlln_critsec_base_port_offset_mappings);
			
			if (xlln_base_port_mappings_original.count(portHBO)) {
				BASE_PORT_OFFSET_MAPPING* mappingOriginal = xlln_base_port_mappings_original[portHBO];
				portOffset = mappingOriginal->offset;
			}
			
			LeaveCriticalSection(&xlln_critsec_base_port_offset_mappings);
		}
		
		portShiftedHBO = xlive_base_port + portOffset;
		SetSockAddrPort(&sockAddrExternal, portShiftedHBO);
	}
	
	if (portShiftedHBO == (IsUsingBasePort(xlive_base_port) ? xlive_base_port : xlive_port_online) && perpetual_socket != xlive_xsocket_perpetual_core_socket) {
		XLLNCloseCoreSocket();
		
		EnterCriticalSection(&xlive_critsec_sockets);
		
		xlive_xsocket_perpetual_core_socket = (SOCKET)CreateMutexA(NULL, false, NULL);
		
		SOCKET transitorySocketTitleSocket = xlive_xsocket_perpetual_to_transitory_socket[perpetual_socket];
		xlive_xsocket_perpetual_to_transitory_socket[xlive_xsocket_perpetual_core_socket] = transitorySocketTitleSocket;
		
		XllnWndSocketsInvalidateSockets();
		
		LeaveCriticalSection(&xlive_critsec_sockets);
		
		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_CONTEXT_XLIVELESSNESS | XLLN_LOG_LEVEL_INFO
			, "%s perpetual_core_socket (0x%zx) now points to socket (P,T) (0x%zx,0x%zx)."
			, __func__
			, xlive_xsocket_perpetual_core_socket
			, perpetual_socket
			, transitorySocketTitleSocket
		);
		
		uint32_t sockOptValue = 1;
		int32_t resultSetSockOpt = XSocketSetSockOpt(xlive_xsocket_perpetual_core_socket, SOL_SOCKET, SO_BROADCAST, (const char*)&sockOptValue, sizeof(sockOptValue));
		if (resultSetSockOpt == SOCKET_ERROR) {
			int32_t errorSetSockOpt = WSAGetLastError();
			XLLN_DEBUG_LOG_ECODE(errorSetSockOpt, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_CONTEXT_XLIVELESSNESS | XLLN_LOG_LEVEL_ERROR
				, "%s Failed to set Broadcast option on perpetual_core_socket."
				, __func__
			);
		}
	}
	
	bool networkAddrAny = false;
	while (1) {
		if (networkAddrAny) {
			((sockaddr_in*)&sockAddrExternal)->sin_addr.s_addr = htonl(INADDR_ANY);
		}
		
		if (sockAddrExternal.ss_family == AF_INET && ((sockaddr_in*)&sockAddrExternal)->sin_addr.s_addr == htonl(INADDR_ANY)) {
			EnterCriticalSection(&xlive_critsec_network_adapter);
			// TODO Should we perhaps not do this unless the user really wants to like if (xlive_init_preferred_network_adapter_name or config) is set?
			if (xlive_network_adapter && xlive_network_adapter->unicastHAddr != INADDR_LOOPBACK) {
				((sockaddr_in*)&sockAddrExternal)->sin_addr.s_addr = htonl(xlive_network_adapter->unicastHAddr);
				networkAddrAny = true;
			}
			LeaveCriticalSection(&xlive_critsec_network_adapter);
		}
	
		SOCKET transitorySocket = XSocketGetTransitorySocket(perpetual_socket);
		if (transitorySocket == INVALID_SOCKET) {
			WSASetLastError(WSAENOTSOCK);
			return SOCKET_ERROR;
		}
		
		SOCKET resultSocketBind = bind(transitorySocket, (const sockaddr*)&sockAddrExternal, sockAddrExternalSize);
		int32_t errorSocketBind = WSAGetLastError();
		
		{
			EnterCriticalSection(&xlive_critsec_sockets);
			
			if (resultSocketBind) {
				if (XSocketPerpetualSocketChangedError_(errorSocketBind, perpetual_socket, transitorySocket)) {
					LeaveCriticalSection(&xlive_critsec_sockets);
					continue;
				}
				LeaveCriticalSection(&xlive_critsec_sockets);
				
				if (errorSocketBind == WSAEADDRINUSE) {
					XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
						, "%s socket (P,T) (0x%zx,0x%zx) bind error, another program has taken port. Base: %hu. Offset: %hu. New-shifted: %hu. Original: %hu."
						, __func__
						, perpetual_socket
						, transitorySocket
						, xlive_base_port
						, portOffset
						, portShiftedHBO
						, portHBO
					);
				}
				else {
					XLLN_DEBUG_LOG_ECODE(errorSocketBind, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
						, "%s bind error on socket (P,T) (0x%zx,0x%zx). Base: %hu. Offset: %hu. New-shifted: %hu. Original: %hu."
						, __func__
						, perpetual_socket
						, transitorySocket
						, xlive_base_port
						, portOffset
						, portShiftedHBO
						, portHBO
					);
				}
				
				WSASetLastError(errorSocketBind);
				return SOCKET_ERROR;
			}
			
			uint8_t resultPerpetualChanged = XSocketPerpetualSocketChanged_(perpetual_socket, transitorySocket);
			if (resultPerpetualChanged) {
				LeaveCriticalSection(&xlive_critsec_sockets);
				if (resultPerpetualChanged == 2) {
					continue;
				}
				WSASetLastError(WSAENOTSOCK);
				return SOCKET_ERROR;
			}
			
			SOCKET_MAPPING_INFO* socketMappingInfo = xlive_socket_info[transitorySocket];
			uint16_t portBindHBO = portHBO;
			
			int32_t resultGetsockname = getsockname(transitorySocket, (sockaddr*)&sockAddrExternal, &sockAddrExternalSize);
			int32_t errorGetsockname = WSAGetLastError();
			if (!resultGetsockname) {
				portBindHBO = GetSockAddrPort(&sockAddrExternal);
			}
			
			socketMappingInfo->portBindHBO = portBindHBO;
			socketMappingInfo->portOgHBO = portHBO;
			if (portShiftedHBO) {
				socketMappingInfo->portOffsetHBO = portOffset;
				xlive_port_offset_sockets[portOffset] = perpetual_socket;
			}
			else {
				socketMappingInfo->portOffsetHBO = (int16_t)-1;
			}
			
			int32_t protocol = socketMappingInfo->protocol;
			bool isVdp = socketMappingInfo->isVdpProtocol;
			
			XllnWndSocketsInvalidateSockets();
			
			LeaveCriticalSection(&xlive_critsec_sockets);
			
			if (resultGetsockname) {
				XLLN_DEBUG_LOG_ECODE(errorGetsockname, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
					, "%s socket (P,T) (0x%zx,0x%zx) getsockname error bind port mapped from %hu to unknown address from getsockname."
					, __func__
					, perpetual_socket
					, transitorySocket
					, portHBO
				);
			}
			
			XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_INFO
				, "%s socket (P,T) (0x%zx,0x%zx). Protocol: %s%d%s. Port: %hu -> %hu."
				, __func__
				, perpetual_socket
				, transitorySocket
				, protocol == IPPROTO_UDP ? "UDP:" : (protocol == IPPROTO_TCP ? "TCP:" : "")
				, protocol
				, isVdp ? " was VDP" : ""
				, portHBO
				, portBindHBO
			);
		}
		
		WSASetLastError(errorSocketBind);
		return resultSocketBind;
	}
}

// #12
int32_t WINAPI XSocketConnect(SOCKET perpetual_socket, const sockaddr* name, int32_t name_size)
{
	TRACE_FX();
	
	SOCKADDR_STORAGE sockAddrExternal;
	int32_t sockAddrExternalSize = (int32_t)sizeof(sockAddrExternal);
	memcpy(&sockAddrExternal, name, sockAddrExternalSize < name_size ? sockAddrExternalSize : name_size);
	
	if (sockAddrExternal.ss_family != AF_INET && sockAddrExternal.ss_family != AF_INET6) {
		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
			, "%s perpetual_socket (0x%zx) connect on unsupported socket address family 0x%04hx."
			, __func__
			, perpetual_socket
			, sockAddrExternal.ss_family
		);
		
		WSASetLastError(WSAEAFNOSUPPORT);
		return SOCKET_ERROR;
	}
	
	if (sockAddrExternal.ss_family == AF_INET) {
		const uint32_t ipv4NBO = ((sockaddr_in*)&sockAddrExternal)->sin_addr.s_addr;
		// This address may (hopefully) be an instanceId.
		const uint32_t ipv4HBO = ntohl(ipv4NBO);
		const uint16_t portHBO = GetSockAddrPort(&sockAddrExternal);
		
		uint32_t resultNetter = NetterEntityGetAddrByInstanceIdPort(&sockAddrExternal, ipv4HBO, portHBO);
		if (resultNetter == ERROR_PORT_NOT_SET) {
			char* sockAddrInfo = GET_SOCKADDR_INFO(&sockAddrExternal);
			XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_DEBUG
				, "%s on perpetual_socket (0x%zx) NetterEntityGetAddrByInstanceIdPort ERROR_PORT_NOT_SET address/instanceId 0x%08x:%hu assuming %s."
				, __func__
				, perpetual_socket
				, ipv4HBO
				, portHBO
				, sockAddrInfo ? sockAddrInfo : ""
			);
			if (sockAddrInfo) {
				free(sockAddrInfo);
			}
		}
		else if (resultNetter) {
			XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
				, "%s on perpetual_socket (0x%zx) NetterEntityGetAddrByInstanceIdPort failed to find address 0x%08x:%hu with error 0x%08x."
				, __func__
				, perpetual_socket
				, ipv4HBO
				, portHBO
				, resultNetter
			);
		}
		else {
			char* sockAddrInfo = GET_SOCKADDR_INFO(&sockAddrExternal);
			XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_DEBUG
				, "%s on perpetual_socket (0x%zx) NetterEntityGetAddrByInstanceIdPort found address/instanceId 0x%08x:%hu as %s."
				, __func__
				, perpetual_socket
				, ipv4HBO
				, portHBO
				, sockAddrInfo ? sockAddrInfo : ""
			);
			if (sockAddrInfo) {
				free(sockAddrInfo);
			}
		}
	}
	else {
		char* sockAddrInfo = GET_SOCKADDR_INFO(&sockAddrExternal);
		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_DEBUG
			, "%s on perpetual_socket (0x%zx) connecting to address %s."
			, __func__
			, perpetual_socket
			, sockAddrInfo ? sockAddrInfo : ""
		);
		if (sockAddrInfo) {
			free(sockAddrInfo);
		}
	}
	
	while (1) {
		SOCKET transitorySocket = XSocketGetTransitorySocket(perpetual_socket);
		if (transitorySocket == INVALID_SOCKET) {
			WSASetLastError(WSAENOTSOCK);
			return SOCKET_ERROR;
		}
		
		int32_t resultConnect = connect(transitorySocket, (const sockaddr*)&sockAddrExternal, sockAddrExternalSize);
		int32_t errorConnect = WSAGetLastError();
		
		if (resultConnect && XSocketPerpetualSocketChangedError(errorConnect, perpetual_socket, transitorySocket)) {
			continue;
		}
		
		if (resultConnect && errorConnect != WSAEWOULDBLOCK) {
			char* sockAddrInfo = GET_SOCKADDR_INFO(&sockAddrExternal);
			XLLN_DEBUG_LOG_ECODE(errorConnect, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
				, "%s connect error on socket (P,T) (0x%zx,0x%zx) to address %s."
				, __func__
				, perpetual_socket
				, transitorySocket
				, sockAddrInfo ? sockAddrInfo : ""
			);
			if (sockAddrInfo) {
				free(sockAddrInfo);
			}
		}
		
		WSASetLastError(errorConnect);
		return resultConnect;
	}
}

// #13
int32_t WINAPI XSocketListen(SOCKET perpetual_socket, int32_t backlog)
{
	TRACE_FX();
	
	while (1) {
		SOCKET transitorySocket = XSocketGetTransitorySocket(perpetual_socket);
		if (transitorySocket == INVALID_SOCKET) {
			WSASetLastError(WSAENOTSOCK);
			return SOCKET_ERROR;
		}
		
		int32_t resultListen = listen(transitorySocket, backlog);
		int32_t errorListen = WSAGetLastError();
		
		if (resultListen && XSocketPerpetualSocketChangedError(errorListen, perpetual_socket, transitorySocket)) {
			continue;
		}
		
		if (resultListen) {
			XLLN_DEBUG_LOG_ECODE(errorListen, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
				, "%s listen error on socket (P,T) (0x%zx,0x%zx)."
				, __func__
				, perpetual_socket
				, transitorySocket
			);
		}
		else {
			XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_DEBUG
				, "%s socket (P,T) (0x%zx,0x%zx). backlog: %d."
				, __func__
				, perpetual_socket
				, transitorySocket
				, backlog
			);
		}
		
		WSASetLastError(errorListen);
		return resultListen;
	}
}

// #14
SOCKET WINAPI XSocketAccept(SOCKET perpetual_socket, sockaddr* sock_addr_xlive, int32_t* sock_addr_xlive_size)
{
	TRACE_FX();
	
	SOCKADDR_STORAGE sockAddrExternal;
	int32_t sockAddrExternalSize = (int32_t)sizeof(sockAddrExternal);
	
	while (1) {
		SOCKET transitorySocket = XSocketGetTransitorySocket(perpetual_socket);
		if (transitorySocket == INVALID_SOCKET) {
			WSASetLastError(WSAENOTSOCK);
			return SOCKET_ERROR;
		}
		
		SOCKET resultAccept = accept(transitorySocket, (sockaddr*)&sockAddrExternal, &sockAddrExternalSize);
		int32_t errorAccept = WSAGetLastError();
		
		if (resultAccept == INVALID_SOCKET && XSocketPerpetualSocketChangedError(errorAccept, perpetual_socket, transitorySocket)) {
			continue;
		}
		
		if (resultAccept == INVALID_SOCKET) {
			if (errorAccept != WSAEWOULDBLOCK) {
				XLLN_DEBUG_LOG_ECODE(errorAccept, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
					, "%s accept error on socket (P,T) (0x%zx,0x%zx)."
					, __func__
					, perpetual_socket
					, transitorySocket
				);
			}
		}
		else {
			SOCKET_MAPPING_INFO* socketMappingInfo = new SOCKET_MAPPING_INFO;
			socketMappingInfo->transitorySocket = resultAccept;
			socketMappingInfo->perpetualSocket = (SOCKET)CreateMutexA(NULL, false, NULL);
			resultAccept = socketMappingInfo->perpetualSocket;
			
			socketMappingInfo->type = SOCK_STREAM;
			socketMappingInfo->protocol = IPPROTO_TCP;
			socketMappingInfo->isVdpProtocol = false;
			socketMappingInfo->tcpRemoteAddress = sockAddrExternal;
			// FIXME SendUnknownUserPacket within TCP on connection established. Do in XSocketConnect / connect or appears in XSocketSelect WRITEABLE.
			socketMappingInfo->tcpRemoteInstanceId = 0;
			socketMappingInfo->tcpRemotePortHBO = 0;
			// TODO Should be client bind port not external port, but this is good enough to get by in some cases.
			socketMappingInfo->tcpRemotePortHBO = GetSockAddrPort(&socketMappingInfo->tcpRemoteAddress);
			
			int32_t resultGetsockname = getsockname(socketMappingInfo->transitorySocket, (sockaddr*)&sockAddrExternal, &sockAddrExternalSize);
			if (!resultGetsockname) {
				socketMappingInfo->portBindHBO = GetSockAddrPort(&sockAddrExternal);
			}
			else {
				int32_t errorGetsockname = WSAGetLastError();
				XLLN_DEBUG_LOG_ECODE(errorGetsockname, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
					, "%s socket (P,T) (0x%zx,0x%zx) getsockname error unknown bind address."
					, __func__
					, socketMappingInfo->perpetualSocket
					, socketMappingInfo->transitorySocket
				);
			}
			
			{
				char* sockAddrInfo = GET_SOCKADDR_INFO(&socketMappingInfo->tcpRemoteAddress);
				XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_INFO
					, "%s new TCP socket (P,T) (0x%zx,0x%zx) created, binded to port (%hu), connected to NetEntity 0x%08x:%hu at address %s."
					, __func__
					, socketMappingInfo->perpetualSocket
					, socketMappingInfo->transitorySocket
					, socketMappingInfo->portBindHBO
					, socketMappingInfo->tcpRemoteInstanceId
					, socketMappingInfo->tcpRemotePortHBO
					, sockAddrInfo ? sockAddrInfo : "?"
				);
				if (sockAddrInfo) {
					free(sockAddrInfo);
					sockAddrInfo = 0;
				}
			}
			
			{
				EnterCriticalSection(&xlive_critsec_sockets);
				
				xlive_socket_info[socketMappingInfo->transitorySocket] = socketMappingInfo;
				xlive_xsocket_perpetual_to_transitory_socket[socketMappingInfo->perpetualSocket] = socketMappingInfo->transitorySocket;
				
				XllnWndSocketsInvalidateSockets();
				
				LeaveCriticalSection(&xlive_critsec_sockets);
			}
			
			sockaddr_in* sockAddrIpv4Xlive = ((sockaddr_in*)sock_addr_xlive);
			sockAddrIpv4Xlive->sin_family = AF_INET;
			sockAddrIpv4Xlive->sin_addr.s_addr = htonl(socketMappingInfo->tcpRemoteInstanceId);
			sockAddrIpv4Xlive->sin_port = htons(socketMappingInfo->tcpRemotePortHBO);
			*sock_addr_xlive_size = sizeof(sockaddr_in);
		}
		
		WSASetLastError(errorAccept);
		return resultAccept;
	}
}

// #15
int32_t WINAPI XSocketSelect(int32_t perpetual_socket_count, fd_set* perpetual_sockets_read, fd_set* perpetual_sockets_write, fd_set* perpetual_sockets_except, const timeval* select_timeout)
{
	TRACE_FX();
	
	std::map<SOCKET, SOCKET> socketPerpetualToTransitory;
	std::map<SOCKET, SOCKET> socketTransitoryToPerpetual;
	
	while (1) {
		socketPerpetualToTransitory.clear();
		socketTransitoryToPerpetual.clear();
		
		// Map all the perpetual sockets to their current transitory sockets.
		bool unknownPerpetual = false;
		{
			EnterCriticalSection(&xlive_critsec_sockets);
			
			for (size_t iSet = 0; iSet < 3; iSet++) {
				fd_set* perpetual_sockets = 0;
				switch (iSet) {
					case 0: {
						perpetual_sockets = perpetual_sockets_read;
						break;
					}
					case 1: {
						perpetual_sockets = perpetual_sockets_write;
						break;
					}
					case 2: {
						perpetual_sockets = perpetual_sockets_except;
						break;
					}
					default: {
						__debugbreak();
					}
				}
				
				if (!perpetual_sockets) {
					continue;
				}
				
				for (u_int iSocket = 0; iSocket < perpetual_sockets->fd_count; iSocket++) {
					SOCKET &perpetual_socket = perpetual_sockets->fd_array[iSocket];
					SOCKET transitorySocket = INVALID_SOCKET;
					const auto &itrTransitorySocket = xlive_xsocket_perpetual_to_transitory_socket.find(perpetual_socket);
					if (itrTransitorySocket != xlive_xsocket_perpetual_to_transitory_socket.end()) {
						transitorySocket = itrTransitorySocket->second;
					}
					if (transitorySocket == INVALID_SOCKET) {
						unknownPerpetual = true;
						XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
							, "%s unknown perpetual_socket (0x%zx)."
							, __func__
							, perpetual_socket
						);
						continue;
					}
					
					socketPerpetualToTransitory[perpetual_socket] = transitorySocket;
					socketTransitoryToPerpetual[transitorySocket] = perpetual_socket;
				}
			}
			
			LeaveCriticalSection(&xlive_critsec_sockets);
		}
		if (unknownPerpetual) {
			WSASetLastError(WSAENOTSOCK);
			return SOCKET_ERROR;
		}
		
		// Overwrite the perpetual_sockets to actually be transitory sockets within as this fd_set is unknown max length and would need to be allocated on the heap if copied in order to use it in the call to select(...).
		for (size_t iSet = 0; iSet < 3; iSet++) {
			fd_set* perpetual_sockets = 0;
			switch (iSet) {
				case 0: {
					perpetual_sockets = perpetual_sockets_read;
					break;
				}
				case 1: {
					perpetual_sockets = perpetual_sockets_write;
					break;
				}
				case 2: {
					perpetual_sockets = perpetual_sockets_except;
					break;
				}
				default: {
					__debugbreak();
				}
			}
			
			if (!perpetual_sockets) {
				continue;
			}
			
			for (u_int iSocket = 0; iSocket < perpetual_sockets->fd_count; iSocket++) {
				SOCKET &perpetual_socket = perpetual_sockets->fd_array[iSocket];
				SOCKET transitorySocket = socketPerpetualToTransitory[perpetual_socket];
				
				XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_DEBUG
					, "%s socket (P,T) (0x%zx,0x%zx) in %s at iSocket (0x%08x)."
					, __func__
					, perpetual_socket
					, transitorySocket
					, (iSet == 0 ? "READ" : (iSet == 1 ? "WRITE" : "EXCEPT"))
					, iSocket
				);
				
				perpetual_socket = transitorySocket;
			}
		}
		
		int32_t resultSelect = select(perpetual_socket_count, perpetual_sockets_read, perpetual_sockets_write, perpetual_sockets_except, select_timeout);
		int32_t errorSelect = WSAGetLastError();
		
		// Revert the Title provided fd_set and check if the error if any is due to socket rebinding such that we need to loop again.
		bool loopAgain = false;
		for (size_t iSet = 0; iSet < 3; iSet++) {
			fd_set* perpetual_sockets = 0;
			switch (iSet) {
				case 0: {
					perpetual_sockets = perpetual_sockets_read;
					break;
				}
				case 1: {
					perpetual_sockets = perpetual_sockets_write;
					break;
				}
				case 2: {
					perpetual_sockets = perpetual_sockets_except;
					break;
				}
				default: {
					__debugbreak();
				}
			}
			
			if (!perpetual_sockets) {
				continue;
			}
			
			for (u_int iSocket = 0; iSocket < perpetual_sockets->fd_count; iSocket++) {
				SOCKET &perpetual_socket = perpetual_sockets->fd_array[iSocket];
				SOCKET transitorySocket = perpetual_socket;
				
				perpetual_socket = socketTransitoryToPerpetual[perpetual_socket];
				
				if (!loopAgain) {
					if (resultSelect == SOCKET_ERROR && (errorSelect == WSAEINTR || errorSelect == WSAENOTSOCK)) {
						EnterCriticalSection(&xlive_critsec_sockets);
						if (xlive_xsocket_perpetual_to_transitory_socket.count(perpetual_socket)
							&& xlive_xsocket_perpetual_to_transitory_socket[perpetual_socket] != transitorySocket
						) {
							loopAgain = true;
						}
						LeaveCriticalSection(&xlive_critsec_sockets);
					}
				}
			}
		}
		if (loopAgain) {
			continue;
		}
		
#ifdef XLLN_DEBUG
		if (resultSelect == SOCKET_ERROR) {
			XLLN_DEBUG_LOG_ECODE(errorSelect, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
				, "%s select error."
				, __func__
			);
		}
		else {
			for (size_t iSet = 0; iSet < 3; iSet++) {
				fd_set* perpetual_sockets = 0;
				switch (iSet) {
					case 0: {
						perpetual_sockets = perpetual_sockets_read;
						break;
					}
					case 1: {
						perpetual_sockets = perpetual_sockets_write;
						break;
					}
					case 2: {
						perpetual_sockets = perpetual_sockets_except;
						break;
					}
					default: {
						__debugbreak();
					}
				}
				
				if (!perpetual_sockets) {
					continue;
				}
				
				for (u_int iSocket = 0; iSocket < perpetual_sockets->fd_count; iSocket++) {
					SOCKET &perpetual_socket = perpetual_sockets->fd_array[iSocket];
					SOCKET transitorySocket = socketPerpetualToTransitory[perpetual_socket];
					
					XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_DEBUG
						, "%s RESULT socket (P,T) (0x%zx,0x%zx) in %s at iSocket (0x%08x)."
						, __func__
						, perpetual_socket
						, transitorySocket
						, (iSet == 0 ? "READ" : (iSet == 1 ? "WRITE" : "EXCEPT"))
						, iSocket
					);
				}
			}
		}
#endif
		
		WSASetLastError(errorSelect);
		return resultSelect;
	}
}

// #18
int32_t WINAPI XSocketRecv(SOCKET perpetual_socket, char* data_buffer, int32_t data_buffer_size, int32_t flags)
{
	TRACE_FX();
	if (flags) {
		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
			, "%s flags value (0x%08x) must be 0 on perpetual_socket (0x%zx)."
			, __func__
			, flags
			, perpetual_socket
		);
		
		WSASetLastError(WSAEOPNOTSUPP);
		return SOCKET_ERROR;
	}
	
	while (1) {
		SOCKET transitorySocket = XSocketGetTransitorySocket(perpetual_socket);
		if (transitorySocket == INVALID_SOCKET) {
			WSASetLastError(WSAENOTSOCK);
			return SOCKET_ERROR;
		}
		
		int32_t resultDataRecvSize = recv(transitorySocket, data_buffer, data_buffer_size, flags);
		int32_t errorRecv = WSAGetLastError();
		
		if (resultDataRecvSize == SOCKET_ERROR && XSocketPerpetualSocketChangedError(errorRecv, perpetual_socket, transitorySocket)) {
			continue;
		}
		
		if (resultDataRecvSize == SOCKET_ERROR) {
			XLLN_DEBUG_LOG_ECODE(errorRecv, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
				, "%s recv error on socket (P,T) (0x%zx,0x%zx)."
				, __func__
				, perpetual_socket
				, transitorySocket
			);
		}
		else {
			XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_DEBUG
				, "%s socket (P,T) (0x%zx,0x%zx) data received size 0x%08x."
				, __func__
				, perpetual_socket
				, transitorySocket
				, resultDataRecvSize
			);
		}
		
		WSASetLastError(errorRecv);
		return resultDataRecvSize;
	}
}

static int32_t xlive_xsocket_large_data_buffer_size = 1024;

// #20
int32_t WINAPI XSocketRecvFrom(SOCKET perpetual_socket, char* data_buffer, int32_t data_buffer_size, int32_t flags, sockaddr* address_from, int32_t* address_from_size)
{
	TRACE_FX();
	if (flags) {
		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
			, "%s flags value (0x%08x) must be 0 on perpetual_socket (0x%zx)."
			, __func__
			, flags
			, perpetual_socket
		);
		
		WSASetLastError(WSAEOPNOTSUPP);
		return SOCKET_ERROR;
	}
	
	uint8_t* largeDataBuffer = 0;
	if (xlive_xsocket_large_data_buffer_size && data_buffer_size > 0 && data_buffer_size < xlive_xsocket_large_data_buffer_size) {
		largeDataBuffer = new uint8_t[xlive_xsocket_large_data_buffer_size];
	}
	
	while (1) {
		bool connectionlessSocket = false;
		SOCKADDR_STORAGE sockAddrExternal;
		int32_t sockAddrExternalSize = (int32_t)sizeof(sockAddrExternal);
		
		SOCKET transitorySocket = XSocketGetTransitorySocket(perpetual_socket, &connectionlessSocket);
		if (transitorySocket == INVALID_SOCKET) {
			if (largeDataBuffer) {
				delete[] largeDataBuffer;
				largeDataBuffer = 0;
			}
			
			WSASetLastError(WSAENOTSOCK);
			return SOCKET_ERROR;
		}
		
		int32_t resultDataRecvSize = recvfrom(
			transitorySocket
			, (connectionlessSocket && largeDataBuffer) ? (char*)largeDataBuffer : data_buffer
			, (connectionlessSocket && largeDataBuffer) ? xlive_xsocket_large_data_buffer_size : data_buffer_size
			, flags
			, connectionlessSocket ? (sockaddr*)&sockAddrExternal : address_from
			, connectionlessSocket ? &sockAddrExternalSize : address_from_size
		);
		int32_t errorRecvFrom = WSAGetLastError();
		
		if (resultDataRecvSize == SOCKET_ERROR && XSocketPerpetualSocketChangedError(errorRecvFrom, perpetual_socket, transitorySocket)) {
			continue;
		}
		
		if (resultDataRecvSize <= 0) {
			if (errorRecvFrom != WSAEWOULDBLOCK) {
				XLLN_DEBUG_LOG_ECODE(errorRecvFrom, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
					, "%s recv error on socket (P,T) (0x%zx,0x%zx)."
					, __func__
					, perpetual_socket
					, transitorySocket
				);
			}
			
			if (largeDataBuffer) {
				delete[] largeDataBuffer;
				largeDataBuffer = 0;
			}
			
			WSASetLastError(errorRecvFrom);
			return resultDataRecvSize;
		}
		
		if (!connectionlessSocket) {
			if (largeDataBuffer) {
				delete[] largeDataBuffer;
				largeDataBuffer = 0;
			}
			
			WSASetLastError(errorRecvFrom);
			return resultDataRecvSize;
		}
		
		int32_t resultDataRecvSizeFromHelper = XSocketRecvFromHelper(
			resultDataRecvSize
			, perpetual_socket
			, largeDataBuffer ? (char*)largeDataBuffer : data_buffer
			, largeDataBuffer ? xlive_xsocket_large_data_buffer_size : data_buffer_size
			, flags
			, &sockAddrExternal
			, sockAddrExternalSize
			, address_from
			, address_from_size
		);
		if (resultDataRecvSizeFromHelper == 0) {
			continue;
		}
		
		if (resultDataRecvSizeFromHelper > 0 && largeDataBuffer) {
			if (resultDataRecvSizeFromHelper > data_buffer_size) {
				memcpy(data_buffer, largeDataBuffer, data_buffer_size);
				
				delete[] largeDataBuffer;
				largeDataBuffer = 0;
				
				WSASetLastError(WSAEMSGSIZE);
				return SOCKET_ERROR;
			}
			
			memcpy(data_buffer, largeDataBuffer, resultDataRecvSizeFromHelper);
		}
		
		if (largeDataBuffer) {
			delete[] largeDataBuffer;
			largeDataBuffer = 0;
		}
		
		WSASetLastError(errorRecvFrom);
		return resultDataRecvSizeFromHelper;
	}
}

// #22
int32_t WINAPI XSocketSend(SOCKET perpetual_socket, const char* data_buffer, int32_t data_buffer_size, int32_t flags)
{
	TRACE_FX();
	if (flags) {
		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
			, "%s flags value (0x%08x) must be 0 on perpetual_socket (0x%zx)."
			, __func__
			, flags
			, perpetual_socket
		);
		
		WSASetLastError(WSAEOPNOTSUPP);
		return SOCKET_ERROR;
	}
	
	while (1) {
		SOCKET transitorySocket = XSocketGetTransitorySocket(perpetual_socket);
		if (transitorySocket == INVALID_SOCKET) {
			WSASetLastError(WSAENOTSOCK);
			return SOCKET_ERROR;
		}
		
		int32_t resultDataSendSize = send(transitorySocket, data_buffer, data_buffer_size, flags);
		int32_t errorSend = WSAGetLastError();
		
		if (resultDataSendSize == SOCKET_ERROR && XSocketPerpetualSocketChangedError(errorSend, perpetual_socket, transitorySocket)) {
			continue;
		}
		
		if (resultDataSendSize == SOCKET_ERROR) {
			XLLN_DEBUG_LOG_ECODE(errorSend, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
				, "%s send error on socket (P,T) (0x%zx,0x%zx)."
				, __func__
				, perpetual_socket
				, transitorySocket
			);
		}
		else {
			XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_DEBUG
				, "%s socket (P,T) (0x%zx,0x%zx) data to send size 0x%08x sent size 0x%08x."
				, __func__
				, perpetual_socket
				, transitorySocket
				, data_buffer_size
				, resultDataSendSize
			);
		}
		
		WSASetLastError(errorSend);
		return resultDataSendSize;
	}
}

// #24
int32_t WINAPI XSocketSendTo(SOCKET perpetual_socket, const char* data_buffer, int32_t data_buffer_size, int32_t flags, sockaddr* address_to, int32_t address_to_size)
{
	TRACE_FX();
	if (flags) {
		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
			, "%s flags value (0x%08x) must be 0 on perpetual_socket (0x%zx)."
			, __func__
			, flags
			, perpetual_socket
		);
		
		WSASetLastError(WSAEOPNOTSUPP);
		return SOCKET_ERROR;
	}
	
	while (1) {
		bool connectionlessSocket = false;
		SOCKET transitorySocket = XSocketGetTransitorySocket(perpetual_socket, &connectionlessSocket);
		if (transitorySocket == INVALID_SOCKET) {
			WSASetLastError(WSAENOTSOCK);
			return SOCKET_ERROR;
		}
		
		if (connectionlessSocket && address_to && address_to_size) {
			break;
		}
		
		int32_t resultDataSendToSize = sendto(transitorySocket, data_buffer, data_buffer_size, flags, address_to, address_to_size);
		int32_t errorSendTo = WSAGetLastError();
		
		if (resultDataSendToSize == SOCKET_ERROR && XSocketPerpetualSocketChangedError(errorSendTo, perpetual_socket, transitorySocket)) {
			continue;
		}
		
		if (resultDataSendToSize == SOCKET_ERROR) {
			XLLN_DEBUG_LOG_ECODE(errorSendTo, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
				, "%s sendto error on socket (P,T) (0x%zx,0x%zx)."
				, __func__
				, perpetual_socket
				, transitorySocket
			);
		}
		else {
			XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_DEBUG
				, "%s socket (P,T) (0x%zx,0x%zx) data to send size 0x%08x sent size 0x%08x."
				, __func__
				, perpetual_socket
				, transitorySocket
				, data_buffer_size
				, resultDataSendToSize
			);
		}
		
		WSASetLastError(errorSendTo);
		return resultDataSendToSize;
	}
	
	const int32_t packetSizeHeaderType = (int32_t)sizeof(XLLNNetPacketType::TYPE);
	
	int32_t newPacketBufferTitleDataSize = packetSizeHeaderType + data_buffer_size;
	
	// Check overflow condition.
	if (newPacketBufferTitleDataSize < 0) {
		WSASetLastError(WSAEMSGSIZE);
		return SOCKET_ERROR;
	}
	
	const uint32_t ipv4NBO = ((sockaddr_in*)address_to)->sin_addr.s_addr;
	// This address may (hopefully) be an instanceId.
	const uint32_t ipv4HBO = ntohl(ipv4NBO);
	
	uint8_t* newPacketBufferTitleData = new uint8_t[newPacketBufferTitleDataSize];
	int32_t newPacketBufferTitleDataAlreadyProcessedOffset = 0;
	
	XLLNNetPacketType::TYPE &newPacketTitleDataType = *(XLLNNetPacketType::TYPE*)&newPacketBufferTitleData[newPacketBufferTitleDataAlreadyProcessedOffset];
	newPacketBufferTitleDataAlreadyProcessedOffset += packetSizeHeaderType;
	newPacketTitleDataType = (((SOCKADDR_STORAGE*)address_to)->ss_family == AF_INET && (ipv4HBO == INADDR_BROADCAST || ipv4HBO == INADDR_ANY)) ? XLLNNetPacketType::tTITLE_BROADCAST_PACKET : XLLNNetPacketType::tTITLE_PACKET;
	
	memcpy(&newPacketBufferTitleData[newPacketBufferTitleDataAlreadyProcessedOffset], data_buffer, data_buffer_size);
	
	int32_t resultDataXllnSendToSize = XllnSocketSendTo(perpetual_socket, (char*)newPacketBufferTitleData, newPacketBufferTitleDataSize, flags, address_to, address_to_size);
	int32_t errorXllnSendTo = WSAGetLastError();
	
	delete[] newPacketBufferTitleData;
	newPacketBufferTitleData = 0;
	
	if (resultDataXllnSendToSize > 0) {
		int32_t unsentDataSize = newPacketBufferTitleDataSize - resultDataXllnSendToSize;
		int32_t newPacketDataSizeDifference = newPacketBufferTitleDataSize - data_buffer_size;
		resultDataXllnSendToSize = data_buffer_size - unsentDataSize;
		if (resultDataXllnSendToSize <= newPacketDataSizeDifference) {
			resultDataXllnSendToSize = 0;
		}
	}
	
	WSASetLastError(errorXllnSendTo);
	return resultDataXllnSendToSize;
}

// #26
uint32_t WINAPI XSocketInet_Addr(const char* ipv4_address)
{
	TRACE_FX();
	
	in_addr resultAddress;
	if (!WS2_32_inet_pton(AF_INET, ipv4_address, &resultAddress)) {
		int32_t errorCode = WSAGetLastError();
		XLLN_DEBUG_LOG_ECODE(errorCode, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
			, "%s inet_pton(\"%s\")."
			, __func__
			, ipv4_address
		);
		WSASetLastError(errorCode);
		return htonl(INADDR_NONE);
	}
	return resultAddress.S_un.S_addr;
}

// #27
int32_t WINAPI XSocketWSAGetLastError()
{
	TRACE_FX();
	int32_t result = WSAGetLastError();
	return result;
}

// #37
unsigned long WINAPI XSocketHTONL(unsigned long host_order)
{
	TRACE_FX();
	unsigned long networkOrder = htonl(host_order);
	return networkOrder;
}

// #38
unsigned short WINAPI XSocketNTOHS(unsigned short network_order)
{
	TRACE_FX();
	unsigned short hostOrder = ntohs(network_order);
	return hostOrder;
}

// #39
unsigned long WINAPI XSocketNTOHL(unsigned long network_order)
{
	TRACE_FX();
	unsigned long hostOrder = ntohl(network_order);
	return hostOrder;
}

// #40
unsigned short WINAPI XSocketHTONS(unsigned short host_order)
{
	TRACE_FX();
	unsigned short networkOrder = htons(host_order);
	return networkOrder;
}

static DWORD WINAPI ThreadCoreSocketRecv(void* lpParam)
{
	TRACE_FX();
	
	SOCKET perpetual_socket = (SOCKET)lpParam;
	
	// The XSocketRecvFrom function already captures XLLN packets so anything else we can just ignore anyway. All we need is a thread to trigger the read from the socket.
	const int32_t dataBufferSize = 1024;
	char dataBuffer[dataBufferSize];
	SOCKADDR_STORAGE sockAddrExternal;
	int32_t sockAddrExternalSize = (int32_t)sizeof(sockAddrExternal);
	while (1) {
		int32_t resultRecvFromDataSize = XSocketRecvFrom(perpetual_socket, dataBuffer, dataBufferSize, 0, (sockaddr*)&sockAddrExternal, &sockAddrExternalSize);
		if (resultRecvFromDataSize > 0) {
			// Empty the socket of queued packets.
			continue;
		}
		if (resultRecvFromDataSize == SOCKET_ERROR) {
			int32_t resultErrorRecvFrom = WSAGetLastError();
			if (resultErrorRecvFrom == WSAEWOULDBLOCK) {
				// Normal error. No more data.
				break;
			}
		}
		// Perhaps log error here.
		break;
	}
	
	return ERROR_SUCCESS;
}

void XLLNCloseCoreSocket()
{
	TRACE_FX();
	
	EnterCriticalSection(&xlive_critsec_sockets);
	
	if (xlive_xsocket_perpetual_core_socket == INVALID_SOCKET) {
		LeaveCriticalSection(&xlive_critsec_sockets);
		return;
	}
	
	SOCKET transitorySocket = xlive_xsocket_perpetual_to_transitory_socket[xlive_xsocket_perpetual_core_socket];
	
	SOCKET_MAPPING_INFO* socketMappingInfo = xlive_socket_info[transitorySocket];
	if (socketMappingInfo->perpetualSocket != xlive_xsocket_perpetual_core_socket) {
		xlive_xsocket_perpetual_to_transitory_socket.erase(xlive_xsocket_perpetual_core_socket);
		XllnWndSocketsInvalidateSockets();
		LeaveCriticalSection(&xlive_critsec_sockets);
		return;
	}
	
	LeaveCriticalSection(&xlive_critsec_sockets);
	
	int32_t resultSocketShutdown = XSocketShutdown(xlive_xsocket_perpetual_core_socket, SD_RECEIVE);
	if (resultSocketShutdown) {
		int32_t errorSocketShutdown = WSAGetLastError();
		XLLN_DEBUG_LOG_ECODE(errorSocketShutdown, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_CONTEXT_XLIVELESSNESS | XLLN_LOG_LEVEL_ERROR
			, "%s Failed to shutdown perpetual_core_socket."
			, __func__
		);
	}
	
	int32_t resultSocketClose = XSocketClose(xlive_xsocket_perpetual_core_socket);
	if (resultSocketClose) {
		int32_t errorSocketClose = WSAGetLastError();
		XLLN_DEBUG_LOG_ECODE(errorSocketClose, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_CONTEXT_XLIVELESSNESS | XLLN_LOG_LEVEL_ERROR
			, "%s Failed to close perpetual_core_socket."
			, __func__
		);
	}
	
	xlive_xsocket_perpetual_core_socket = INVALID_SOCKET;
}

static void XLLNCreateCoreSocket()
{
	TRACE_FX();
	
	XLLNCloseCoreSocket();
	
	if (xlive_netsocket_abort) {
		return;
	}
	
	xlive_xsocket_perpetual_core_socket = XSocketCreate(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (xlive_xsocket_perpetual_core_socket == INVALID_SOCKET) {
		int32_t errorSocketCreate = WSAGetLastError();
		XLLN_DEBUG_LOG_ECODE(errorSocketCreate, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_CONTEXT_XLIVELESSNESS | XLLN_LOG_LEVEL_ERROR
			, "%s Failed to create perpetual_core_socket."
			, __func__
		);
		return;
	}
	
	XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_CONTEXT_XLIVELESSNESS | XLLN_LOG_LEVEL_INFO
		, "%s perpetual_core_socket (0x%zx) created."
		, __func__
		, xlive_xsocket_perpetual_core_socket
	);
	
	uint32_t sockOptValue = 1;
	int32_t resultSetSockOpt = XSocketSetSockOpt(xlive_xsocket_perpetual_core_socket, SOL_SOCKET, SO_BROADCAST, (const char*)&sockOptValue, sizeof(sockOptValue));
	if (resultSetSockOpt == SOCKET_ERROR) {
		int errorSetSockOpt = WSAGetLastError();
		XLLN_DEBUG_LOG_ECODE(errorSetSockOpt, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_CONTEXT_XLIVELESSNESS | XLLN_LOG_LEVEL_ERROR
			, "%s Failed to set Broadcast option on perpetual_core_socket."
			, __func__
		);
		return;
	}
	
	sockaddr_in socketBindAddress;
	socketBindAddress.sin_family = AF_INET;
	socketBindAddress.sin_addr.s_addr = htonl(INADDR_ANY);
	if (IsUsingBasePort(xlive_base_port)) {
		socketBindAddress.sin_port = htons(xlive_base_port);
	}
	else {
		socketBindAddress.sin_port = htons(xlive_port_online);
	}
	SOCKET resultSocketBind = XSocketBind(xlive_xsocket_perpetual_core_socket, (sockaddr*)&socketBindAddress, sizeof(socketBindAddress));
	if (resultSocketBind) {
		int errorSocketBind = WSAGetLastError();
		XLLN_DEBUG_LOG_ECODE(errorSocketBind, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_CONTEXT_XLIVELESSNESS | XLLN_LOG_LEVEL_ERROR
			, "%s Failed to bind perpetual_core_socket to port %hu."
			, __func__
			, ntohs(socketBindAddress.sin_port)
		);
		return;
	}
	
	CreateThread(0, 0, ThreadCoreSocketRecv, (void*)xlive_xsocket_perpetual_core_socket, 0, 0);
}

bool InitXSocket()
{
	TRACE_FX();
	
	XLLNCreateCoreSocket();
	XllnThreadKeepAliveStart();
	XLiveThreadQosStart();
	
	return true;
}

bool UninitXSocket()
{
	TRACE_FX();
	
	XLiveThreadQosStop();
	XLLNCloseCoreSocket();
	XllnThreadKeepAliveStop();
	
	return true;
}
