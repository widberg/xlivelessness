#include <winsock2.h>
#include "xdefs.hpp"
#include "xfuncs.hpp"
#include "xwsa.hpp"
#include "xnet.hpp"
#include "xsocket.hpp"
#include "net-entity.hpp"
#include "packet-handler.hpp"
#include "../utils/util-socket.hpp"
#include "../xlln/debug-log.hpp"
#include "../xlln/xlln.hpp"

typedef struct _XWSA_BUFFER {
	uint8_t* dataBuffer = 0;
	uint32_t dataBufferSize = 0;
	uint32_t dataSize = 0;
} XWSA_BUFFER;

typedef struct WSA_OVERLAPPED_SOCKET_INFO_ {
	WSA_OVERLAPPED_SOCKET_INFO_() {
		InitializeCriticalSection(&mutexInProgress);
		memset(&sockAddrExternal, 0, sizeof(sockAddrExternal));
		memset(&SEND_RECV, 0, sizeof(SEND_RECV));
	}
	~WSA_OVERLAPPED_SOCKET_INFO_() {
		DeleteCriticalSection(&mutexInProgress);
		
		if (isRecv) {
			if (SEND_RECV.RECV.dataBufferInternal) {
				delete[] SEND_RECV.RECV.dataBufferInternal;
				SEND_RECV.RECV.dataBufferInternal = 0;
			}
			SEND_RECV.RECV.dataBufferInternalSize = 0;
		}
		else {
			for (uint32_t iBuf = 0; iBuf < SEND_RECV.SEND.dataBuffersInternalCount; iBuf++) {
				delete[] SEND_RECV.SEND.dataBuffersInternal[iBuf].dataBuffer;
				SEND_RECV.SEND.dataBuffersInternal[iBuf].dataBufferSize = 0;
			}
			delete[] SEND_RECV.SEND.dataBuffersInternal;
			SEND_RECV.SEND.dataBuffersInternalCount = 0;
		}
	}

	SOCKET perpetualSocket = INVALID_SOCKET;
	// If set, dataBufferXlive and wsaOverlapped may no longer be valid memory.
	bool hasCompleted = false;
	
	CRITICAL_SECTION mutexInProgress;
	
	// Note that this can potentially be null.
	WSAOVERLAPPED* wsaOverlapped = 0;
	
	union {
		struct {
			uint8_t* dataBufferInternal;
			uint32_t dataBufferInternalSize;
			uint8_t* dataBufferXlive;
			uint32_t dataBufferXliveSize;
			sockaddr* sockAddrXlive;
			int* sockAddrXliveSize;
		} RECV;
		struct {
			XWSA_BUFFER* dataBuffersInternal;
			uint32_t dataBuffersInternalCount;
			uint32_t dataBuffersCount;
		} SEND;
	} SEND_RECV;
	bool isRecv;
	
	SOCKADDR_STORAGE sockAddrExternal;
	// Set when the buffer has already been processed in a previous call to XWSAGetOverlappedResult.
	uint32_t dataBufferSizeResult = 0;
	uint32_t dataTransferred = 0;
	uint32_t socketFlagReturned = 0;
	
} WSA_OVERLAPPED_SOCKET_INFO;

CRITICAL_SECTION xlive_critsec_overlapped_sockets;
// Key: perpetualSocket.
static std::map<SOCKET, WSA_OVERLAPPED_SOCKET_INFO*> xlive_overlapped_sockets_recv;
// Key: perpetualSocket.
static std::map<SOCKET, WSA_OVERLAPPED_SOCKET_INFO*> xlive_overlapped_sockets_send;

// #1
int32_t WINAPI XWSAStartup(uint16_t version_requested, WSADATA* wsa_data)
{
	TRACE_FX();
	
	int32_t result = WSAStartup(version_requested, wsa_data);
	if (result != ERROR_SUCCESS) {
		XLLN_DEBUG_LOG_ECODE(result, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
			, "%s WSAStartup(0x%04hx, _)."
			, __func__
			, version_requested
		);
		return result;
	}
	
	if (!xlive_net_initialized) {
		result = XNetStartup(0);
		if (result != ERROR_SUCCESS) {
			XLLN_DEBUG_LOG_ECODE(result, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
				, "%s XNetStartup."
				, __func__
			);
			return result;
		}
	}
	
	return ERROR_SUCCESS;
}

// #2
int32_t WINAPI XWSACleanup()
{
	TRACE_FX();
	int32_t result = WSACleanup();
	if (result != ERROR_SUCCESS) {
		int32_t errorWSACleanup = WSAGetLastError();
		XLLN_DEBUG_LOG_ECODE(errorWSACleanup, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
			, "%s WSACleanup."
			, __func__
		);
		WSASetLastError(errorWSACleanup);
		return SOCKET_ERROR;
	}
	return ERROR_SUCCESS;
}

static BOOL WINAPI XWSAGetOverlappedResultInternal(SOCKET perpetual_socket, WSAOVERLAPPED* wsa_overlapped, uint32_t* transferred_size, BOOL wait_for_completion, uint32_t* flags)
{
	TRACE_FX();
	
	while (1) {
		SOCKET transitorySocket = XSocketGetTransitorySocket(perpetual_socket);
		if (transitorySocket == INVALID_SOCKET) {
			WSASetLastError(WSAENOTSOCK);
			return SOCKET_ERROR;
		}
		
		BOOL resultGetOverlappedResult = WSAGetOverlappedResult(transitorySocket, wsa_overlapped, (DWORD*)transferred_size, wait_for_completion, (DWORD*)flags);
		int32_t errorGetOverlappedResult = WSAGetLastError();
		
		if (!resultGetOverlappedResult && XSocketPerpetualSocketChangedError(errorGetOverlappedResult, perpetual_socket, transitorySocket)) {
			continue;
		}
		
		if (!resultGetOverlappedResult && !wait_for_completion && errorGetOverlappedResult != WSA_IO_INCOMPLETE) {
			XLLN_DEBUG_LOG_ECODE(errorGetOverlappedResult, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
				, "%s GetOverlappedResult error on socket (P,T) (0x%zx,0x%zx)."
				, __func__
				, perpetual_socket
				, transitorySocket
			);
		}
		
		WSASetLastError(errorGetOverlappedResult);
		return resultGetOverlappedResult;
	}
}

// #16
BOOL WINAPI XWSAGetOverlappedResult(SOCKET perpetual_socket, WSAOVERLAPPED* wsa_overlapped, uint32_t* transferred_size, BOOL wait_for_completion, uint32_t* flags)
{
	TRACE_FX();
	
	bool foundSocket = false;
	bool foundOverlapped = false;
	BOOL resultGetOverlappedResult = FALSE;
	int32_t errorGetOverlappedResult = WSAEINVAL;
	
	{
		EnterCriticalSection(&xlive_critsec_overlapped_sockets);
		
		bool reloop;
		bool isRecv = false;
		do {
			reloop = false;
			auto &xlive_overlapped_sockets = (isRecv ? xlive_overlapped_sockets_recv : xlive_overlapped_sockets_send);
			if (xlive_overlapped_sockets.count(perpetual_socket)) {
				foundSocket = true;
				WSA_OVERLAPPED_SOCKET_INFO* wsaOverlappedInfo = xlive_overlapped_sockets[perpetual_socket];
				
				if (wsaOverlappedInfo->wsaOverlapped == wsa_overlapped) {
					foundOverlapped = true;
					
					if (wsaOverlappedInfo->hasCompleted) {
						*transferred_size = wsaOverlappedInfo->dataTransferred;
						*flags = wsaOverlappedInfo->socketFlagReturned;
						
						if (isRecv) {
							XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_DEBUG
								, "%s perpetual_socket (0x%zx) data recv already read completed size 0x%x data for title size 0x%x."
								, __func__
								, perpetual_socket
								, wsaOverlappedInfo->dataBufferSizeResult
								, wsaOverlappedInfo->dataTransferred
							);
						}
						else {
							XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_DEBUG
								, "%s perpetual_socket (0x%zx) data send already read completed data to send size 0x%x sent wrapped size 0x%x."
								, __func__
								, perpetual_socket
								, wsaOverlappedInfo->dataTransferred
								, wsaOverlappedInfo->dataBufferSizeResult
							);
						}
						
						LeaveCriticalSection(&xlive_critsec_overlapped_sockets);
					}
					else {
						
						{
							BOOL resultInProgress = TryEnterCriticalSection(&wsaOverlappedInfo->mutexInProgress);
							if (resultInProgress == FALSE) {
								XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
									, "%s perpetual_socket (0x%zx) failed to enter mutexInProgress critsec."
									, __func__
									, perpetual_socket
								);
								errorGetOverlappedResult = WSAEALREADY;
								resultGetOverlappedResult = FALSE;
								
								LeaveCriticalSection(&xlive_critsec_overlapped_sockets);
								break;
							}
						}
						
						LeaveCriticalSection(&xlive_critsec_overlapped_sockets);
						
						resultGetOverlappedResult = XWSAGetOverlappedResultInternal(
							perpetual_socket
							, wsa_overlapped
							, &wsaOverlappedInfo->dataBufferSizeResult
							, wait_for_completion
							, &wsaOverlappedInfo->socketFlagReturned
						);
						errorGetOverlappedResult = WSAGetLastError();
						
						if (isRecv) {
							if (resultGetOverlappedResult) {
								int32_t resultDataRecvSizeFromHelper = XSocketRecvFromHelper(
									(int)wsaOverlappedInfo->dataBufferSizeResult
									, perpetual_socket
									, (char*)wsaOverlappedInfo->SEND_RECV.RECV.dataBufferInternal
									, wsaOverlappedInfo->SEND_RECV.RECV.dataBufferInternalSize
									, wsaOverlappedInfo->socketFlagReturned
									, &wsaOverlappedInfo->sockAddrExternal
									, sizeof(wsaOverlappedInfo->sockAddrExternal)
									, wsaOverlappedInfo->SEND_RECV.RECV.sockAddrXlive
									, wsaOverlappedInfo->SEND_RECV.RECV.sockAddrXliveSize
								);
								if (resultDataRecvSizeFromHelper < 0) {
									resultDataRecvSizeFromHelper = 0;
								}
								
								wsaOverlappedInfo->dataTransferred = resultDataRecvSizeFromHelper;
								
								*transferred_size = wsaOverlappedInfo->dataTransferred;
								*flags = wsaOverlappedInfo->socketFlagReturned;
								
								if (wsaOverlappedInfo->dataTransferred > wsaOverlappedInfo->SEND_RECV.RECV.dataBufferXliveSize) {
									memcpy(wsaOverlappedInfo->SEND_RECV.RECV.dataBufferXlive, wsaOverlappedInfo->SEND_RECV.RECV.dataBufferInternal, wsaOverlappedInfo->SEND_RECV.RECV.dataBufferXliveSize);
									
									wsaOverlappedInfo->hasCompleted = true;
									errorGetOverlappedResult = WSAEMSGSIZE;
									resultGetOverlappedResult = FALSE;
									
									XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
										, "%s perpetual_socket (0x%zx) data recv size (0x%08x) data for title size 0x%x too big for title buffer size 0x%x."
										, __func__
										, perpetual_socket
										, wsaOverlappedInfo->dataBufferSizeResult
										, wsaOverlappedInfo->dataTransferred
										, wsaOverlappedInfo->SEND_RECV.RECV.dataBufferXliveSize
									);
								}
								else {
									memcpy(wsaOverlappedInfo->SEND_RECV.RECV.dataBufferXlive, wsaOverlappedInfo->SEND_RECV.RECV.dataBufferInternal, wsaOverlappedInfo->dataTransferred);
									
									wsaOverlappedInfo->hasCompleted = true;
									
									XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_DEBUG
										, "%s perpetual_socket (0x%zx) data recv size 0x%x data for title size 0x%x."
										, __func__
										, perpetual_socket
										, wsaOverlappedInfo->dataBufferSizeResult
										, wsaOverlappedInfo->dataTransferred
									);
								}
							}
							// TODO if cancelled by rebinding sockets.
							//else if (!resultGetOverlappedResult && errorGetOverlappedResult == ERROR_OPERATION_ABORTED && wsaOverlappedInfo->hasCompleted) {
							//}
							else if (!resultGetOverlappedResult && errorGetOverlappedResult != WSA_IO_INCOMPLETE) {
								wsaOverlappedInfo->hasCompleted = true;
								
								XLLN_DEBUG_LOG_ECODE(errorGetOverlappedResult, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
									, "%s perpetual_socket (0x%zx) recv failed."
									, __func__
									, perpetual_socket
								);
							}
						}
						else {
							if (resultGetOverlappedResult) {
								*transferred_size = wsaOverlappedInfo->dataTransferred;
								*flags = wsaOverlappedInfo->socketFlagReturned;
								
								XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_DEBUG
									, "%s perpetual_socket (0x%zx) data to send size 0x%x sent wrapped size 0x%x."
									, __func__
									, perpetual_socket
									, wsaOverlappedInfo->dataTransferred
									, wsaOverlappedInfo->dataBufferSizeResult
								);
								
								wsaOverlappedInfo->hasCompleted = true;
							}
							else if (!resultGetOverlappedResult && errorGetOverlappedResult != WSA_IO_INCOMPLETE) {
								wsaOverlappedInfo->hasCompleted = true;
								
								XLLN_DEBUG_LOG_ECODE(errorGetOverlappedResult, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
									, "%s perpetual_socket (0x%zx) send failed."
									, __func__
									, perpetual_socket
								);
							}
						}
						
						LeaveCriticalSection(&wsaOverlappedInfo->mutexInProgress);
					}
					
					break;
				}
			}
		} while (reloop || (isRecv = !isRecv));
		
		if (!foundOverlapped) {
			LeaveCriticalSection(&xlive_critsec_overlapped_sockets);
		}
	}
	
	if (!foundSocket) {
		WSASetLastError(WSAENOTSOCK);
		return FALSE;
	}
	if (!foundOverlapped) {
		WSASetLastError(WSAECANCELLED);
		return FALSE;
	}
	
	WSASetLastError(errorGetOverlappedResult);
	return resultGetOverlappedResult;
}

// Since our socket handles are not file handles, apps can NOT call CancelIO API to cancel
// outstanding overlapped I/O requests. Apps must call WSACancelOverlappedIO function instead.
// #17
int32_t WINAPI XWSACancelOverlappedIO(SOCKET perpetual_socket)
{
	TRACE_FX();
	if (perpetual_socket == INVALID_SOCKET) {
		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR, "%s perpetual_socket is INVALID_SOCKET.", __func__);
		WSASetLastError(WSAENOTSOCK);
		return SOCKET_ERROR;
	}
	SOCKET transitorySocket = XSocketGetTransitorySocket(perpetual_socket);
	if (transitorySocket == INVALID_SOCKET) {
		WSASetLastError(WSAENOTSOCK);
		return SOCKET_ERROR;
	}
	
	bool foundSocket = false;
	int32_t errorCancelOverlapped = WSASYSCALLFAILURE;
	int32_t resultCancelOverlapped = SOCKET_ERROR;
	
	{
		EnterCriticalSection(&xlive_critsec_overlapped_sockets);
		
		bool isRecv = false;
		do {
			auto &xlive_overlapped_sockets = (isRecv ? xlive_overlapped_sockets_recv : xlive_overlapped_sockets_send);
			if (xlive_overlapped_sockets.count(perpetual_socket)) {
				foundSocket = true;
				WSA_OVERLAPPED_SOCKET_INFO* wsaOverlappedInfo = xlive_overlapped_sockets[perpetual_socket];
				
				if (wsaOverlappedInfo->wsaOverlapped && wsaOverlappedInfo->wsaOverlapped->hEvent && wsaOverlappedInfo->wsaOverlapped->hEvent != INVALID_HANDLE_VALUE) {
					SetEvent(wsaOverlappedInfo->wsaOverlapped->hEvent);
				}
				
				wsaOverlappedInfo->hasCompleted = true;
				
				// Rebind this socket so the kernel cancels / returns from any blocking calls.
				//XSocketRebindSockets_(&perpetual_socket, 1);
				
				BOOL resultCancelIO = CancelIoEx((HANDLE)transitorySocket, 0);
				uint32_t errorCancelIo = GetLastError();
				
				{
					EnterCriticalSection(&wsaOverlappedInfo->mutexInProgress);
					LeaveCriticalSection(&wsaOverlappedInfo->mutexInProgress);
				}
				
				errorCancelOverlapped = ERROR_SUCCESS;
				resultCancelOverlapped = ERROR_SUCCESS;
				
				break;
			}
		} while (isRecv = !isRecv);
		
		LeaveCriticalSection(&xlive_critsec_overlapped_sockets);
	}
	
	if (!foundSocket) {
		errorCancelOverlapped = WSAENOTSOCK;
		resultCancelOverlapped = SOCKET_ERROR;
	}
	
	WSASetLastError(errorCancelOverlapped);
	return resultCancelOverlapped;
}

void XWSADeleteOverlappedSocket_(SOCKET perpetual_socket, SOCKET transitory_socket)
{
	EnterCriticalSection(&xlive_critsec_overlapped_sockets);
	
	bool isRecv = false;
	do {
		auto &xlive_overlapped_sockets = (isRecv ? xlive_overlapped_sockets_recv : xlive_overlapped_sockets_send);
		if (xlive_overlapped_sockets.count(perpetual_socket)) {
			WSA_OVERLAPPED_SOCKET_INFO* wsaOverlappedInfo = xlive_overlapped_sockets[perpetual_socket];
			
			if (wsaOverlappedInfo->wsaOverlapped && wsaOverlappedInfo->wsaOverlapped->hEvent && wsaOverlappedInfo->wsaOverlapped->hEvent != INVALID_HANDLE_VALUE) {
				SetEvent(wsaOverlappedInfo->wsaOverlapped->hEvent);
			}
			
			wsaOverlappedInfo->hasCompleted = true;
			
			BOOL resultCancelIO = CancelIoEx((HANDLE)transitory_socket, 0);
			uint32_t errorCancelIo = GetLastError();
			
			{
				EnterCriticalSection(&wsaOverlappedInfo->mutexInProgress);
				LeaveCriticalSection(&wsaOverlappedInfo->mutexInProgress);
			}
			
			xlive_overlapped_sockets.erase(perpetual_socket);
			delete wsaOverlappedInfo;
		}
	} while (isRecv = !isRecv);
	
	LeaveCriticalSection(&xlive_critsec_overlapped_sockets);
}

// #19
int32_t WINAPI XWSARecv(
	SOCKET perpetual_socket
	, WSABUF* wsa_buffers
	, uint32_t wsa_buffer_count
	, uint32_t* received_size
	, uint32_t* flags
	, WSAOVERLAPPED* wsa_overlapped
	, LPWSAOVERLAPPED_COMPLETION_ROUTINE wsa_completion_routine
)
{
	TRACE_FX();
	if (!flags || *flags) {
		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
			, "%s flags (0x%08x) must be 0 on perpetual_socket (0x%zx)."
			, __func__
			, *flags
			, perpetual_socket
		);
		
		WSASetLastError(WSAEOPNOTSUPP);
		return SOCKET_ERROR;
	}
	if (wsa_buffer_count != 1) {
		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
			, "%s wsa_buffer_count (%u) must be 1 on perpetual_socket (0x%zx)."
			, __func__
			, wsa_buffer_count
			, perpetual_socket
		);
		
		WSASetLastError(WSAEOPNOTSUPP);
		return SOCKET_ERROR;
	}
	if (wsa_completion_routine) {
		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
			, "%s wsa_completion_routine must be NULL on perpetual_socket (0x%zx)."
			, __func__
			, perpetual_socket
		);
		
		WSASetLastError(WSAEOPNOTSUPP);
		return SOCKET_ERROR;
	}
	
	while (1) {
		SOCKET transitorySocket = XSocketGetTransitorySocket(perpetual_socket);
		if (transitorySocket == INVALID_SOCKET) {
			WSASetLastError(WSAENOTSOCK);
			return SOCKET_ERROR;
		}
		
		int32_t resultRecv = WSARecv(transitorySocket, wsa_buffers, wsa_buffer_count, (DWORD*)received_size, (DWORD*)flags, wsa_overlapped, wsa_completion_routine);
		int32_t errorRecv = WSAGetLastError();
		
		if (resultRecv == SOCKET_ERROR && errorRecv != WSA_IO_PENDING && XSocketPerpetualSocketChangedError(errorRecv, perpetual_socket, transitorySocket)) {
			continue;
		}
		
		if (resultRecv != ERROR_SUCCESS && errorRecv != WSA_IO_PENDING) {
			XLLN_DEBUG_LOG_ECODE(errorRecv, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
				, "%s recv error on socket (P,T) (0x%zx,0x%zx)."
				, __func__
				, perpetual_socket
				, transitorySocket
			);
		}
		
		WSASetLastError(errorRecv);
		return resultRecv;
	}
}

// #21
int32_t WINAPI XWSARecvFrom(
	SOCKET perpetual_socket
	, WSABUF* wsa_buffers
	, uint32_t wsa_buffer_count
	, uint32_t* received_size
	, uint32_t* flags
	, sockaddr* address_from
	, int32_t* address_from_size
	, WSAOVERLAPPED* wsa_overlapped
	, LPWSAOVERLAPPED_COMPLETION_ROUTINE wsa_completion_routine
)
{
	TRACE_FX();
	if (!flags) {
		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
			, "%s flags must not be NULL on perpetual_socket (0x%zx)."
			, __func__
			, perpetual_socket
		);
		
		WSASetLastError(WSAEOPNOTSUPP);
		return SOCKET_ERROR;
	}
	if (wsa_buffer_count != 1) {
		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
			, "%s wsa_buffer_count (%u) must be 1 on perpetual_socket (0x%zx)."
			, __func__
			, wsa_buffer_count
			, perpetual_socket
		);
		
		WSASetLastError(WSAEOPNOTSUPP);
		return SOCKET_ERROR;
	}
	if (wsa_completion_routine) {
		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
			, "%s wsa_completion_routine must be NULL on perpetual_socket (0x%zx)."
			, __func__
			, perpetual_socket
		);
		
		WSASetLastError(WSAEOPNOTSUPP);
		return SOCKET_ERROR;
	}
	
	WSA_OVERLAPPED_SOCKET_INFO* wsaOverlappedInfo = 0;
	{
		EnterCriticalSection(&xlive_critsec_overlapped_sockets);
		
		if (xlive_overlapped_sockets_recv.count(perpetual_socket)) {
			wsaOverlappedInfo = xlive_overlapped_sockets_recv[perpetual_socket];
		}
		else {
			wsaOverlappedInfo = new WSA_OVERLAPPED_SOCKET_INFO;
			wsaOverlappedInfo->perpetualSocket = perpetual_socket;
			wsaOverlappedInfo->isRecv = true;
			xlive_overlapped_sockets_recv[perpetual_socket] = wsaOverlappedInfo;
			
			wsaOverlappedInfo->SEND_RECV.RECV.dataBufferInternalSize = 1024;
			wsaOverlappedInfo->SEND_RECV.RECV.dataBufferInternal = new uint8_t[wsaOverlappedInfo->SEND_RECV.RECV.dataBufferInternalSize];
			wsaOverlappedInfo->hasCompleted = true;
		}
		
		if (!wsaOverlappedInfo->hasCompleted) {
			LeaveCriticalSection(&xlive_critsec_overlapped_sockets);
			
			XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
				, "%s an existing Overlapped Recv operation is already in progress on perpetual_socket (0x%zx)."
				, __func__
				, perpetual_socket
			);
			
			WSASetLastError(WSAEINPROGRESS);
			return SOCKET_ERROR;
		}
		
		wsaOverlappedInfo->hasCompleted = false;
		wsaOverlappedInfo->wsaOverlapped = wsa_overlapped;
		wsaOverlappedInfo->dataBufferSizeResult = 0;
		wsaOverlappedInfo->dataTransferred = 0;
		wsaOverlappedInfo->socketFlagReturned = 0;
		wsaOverlappedInfo->SEND_RECV.RECV.dataBufferXliveSize = wsa_buffers[0].len;
		wsaOverlappedInfo->SEND_RECV.RECV.dataBufferXlive = (uint8_t*)wsa_buffers[0].buf;
		wsaOverlappedInfo->SEND_RECV.RECV.sockAddrXlive = address_from;
		wsaOverlappedInfo->SEND_RECV.RECV.sockAddrXliveSize = address_from_size;
		
		LeaveCriticalSection(&xlive_critsec_overlapped_sockets);
	}
	
	if (wsaOverlappedInfo->SEND_RECV.RECV.dataBufferInternalSize < wsaOverlappedInfo->SEND_RECV.RECV.dataBufferXliveSize) {
		delete[] wsaOverlappedInfo->SEND_RECV.RECV.dataBufferInternal;
		wsaOverlappedInfo->SEND_RECV.RECV.dataBufferInternalSize = wsaOverlappedInfo->SEND_RECV.RECV.dataBufferXliveSize;
		wsaOverlappedInfo->SEND_RECV.RECV.dataBufferInternal = new uint8_t[wsaOverlappedInfo->SEND_RECV.RECV.dataBufferInternalSize];
	}
	
	while (1) {
		bool connectionlessSocket = false;
		WSABUF bufferInternal;
		bufferInternal.buf = (CHAR*)wsaOverlappedInfo->SEND_RECV.RECV.dataBufferInternal;
		bufferInternal.len = wsaOverlappedInfo->SEND_RECV.RECV.dataBufferInternalSize;
		
		SOCKET transitorySocket = XSocketGetTransitorySocket(perpetual_socket, &connectionlessSocket);
		if (transitorySocket == INVALID_SOCKET) {
			wsaOverlappedInfo->hasCompleted = true;
			WSASetLastError(WSAENOTSOCK);
			return SOCKET_ERROR;
		}
		
		int32_t sockAddrExternalSize = sizeof(wsaOverlappedInfo->sockAddrExternal);
		int32_t resultRecvFrom = WSARecvFrom(
			transitorySocket
			, connectionlessSocket ? &bufferInternal : wsa_buffers
			, wsa_buffer_count
			, (DWORD*)&(wsaOverlappedInfo->dataBufferSizeResult)
			, (DWORD*)flags
			, connectionlessSocket ? (sockaddr*)&wsaOverlappedInfo->sockAddrExternal : address_from
			, connectionlessSocket ? &sockAddrExternalSize : address_from_size
			, wsa_overlapped
			, wsa_completion_routine
		);
		int32_t errorRecvFrom = WSAGetLastError();
		
		if (resultRecvFrom == SOCKET_ERROR && errorRecvFrom != WSA_IO_PENDING && XSocketPerpetualSocketChangedError(errorRecvFrom, perpetual_socket, transitorySocket)) {
			continue;
		}
		
		if (resultRecvFrom != ERROR_SUCCESS && errorRecvFrom != WSA_IO_PENDING) {
			XLLN_DEBUG_LOG_ECODE(errorRecvFrom, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
				, "%s recvfrom error on socket (P,T) (0x%zx,0x%zx)."
				, __func__
				, perpetual_socket
				, transitorySocket
			);
			
			wsaOverlappedInfo->hasCompleted = true;
			WSASetLastError(errorRecvFrom);
			return resultRecvFrom;
		}
		
		if (resultRecvFrom == ERROR_SUCCESS && wsaOverlappedInfo->dataBufferSizeResult == 0) {
			if (received_size) {
				*received_size = wsaOverlappedInfo->dataTransferred;
			}
			
			wsaOverlappedInfo->hasCompleted = true;
			WSASetLastError(errorRecvFrom);
			return resultRecvFrom;
		}
		
		if (!connectionlessSocket) {
			
			if (resultRecvFrom == ERROR_SUCCESS) {
				if (received_size) {
					*received_size = wsaOverlappedInfo->dataBufferSizeResult;
				}
				
				wsaOverlappedInfo->hasCompleted = true;
			}
			
			WSASetLastError(errorRecvFrom);
			return resultRecvFrom;
		}
		
		if (resultRecvFrom == ERROR_SUCCESS) {
			int32_t resultDataRecvSizeFromHelper = XSocketRecvFromHelper(
				(int32_t)wsaOverlappedInfo->dataBufferSizeResult
				, perpetual_socket
				, (char*)wsaOverlappedInfo->SEND_RECV.RECV.dataBufferInternal
				, wsaOverlappedInfo->SEND_RECV.RECV.dataBufferInternalSize
				, *flags
				, &wsaOverlappedInfo->sockAddrExternal
				, sockAddrExternalSize
				, address_from
				, address_from_size
			);
			if (resultDataRecvSizeFromHelper <= 0) {
				continue;
			}
			
			wsaOverlappedInfo->dataTransferred = resultDataRecvSizeFromHelper;
			
			if (received_size) {
				*received_size = wsaOverlappedInfo->dataTransferred;
			}
			
			if (wsaOverlappedInfo->dataTransferred > wsaOverlappedInfo->SEND_RECV.RECV.dataBufferXliveSize) {
				memcpy(wsaOverlappedInfo->SEND_RECV.RECV.dataBufferXlive, wsaOverlappedInfo->SEND_RECV.RECV.dataBufferInternal, wsaOverlappedInfo->SEND_RECV.RECV.dataBufferXliveSize);
				
				wsaOverlappedInfo->hasCompleted = true;
				WSASetLastError(WSAEMSGSIZE);
				return SOCKET_ERROR;
			}
			
			memcpy(wsaOverlappedInfo->SEND_RECV.RECV.dataBufferXlive, wsaOverlappedInfo->SEND_RECV.RECV.dataBufferInternal, wsaOverlappedInfo->dataTransferred);
			
			wsaOverlappedInfo->hasCompleted = true;
		}
		
		WSASetLastError(errorRecvFrom);
		return resultRecvFrom;
	}
}

// #23
int32_t WINAPI XWSASend(
	SOCKET perpetual_socket
	, WSABUF* wsa_buffers
	, uint32_t wsa_buffer_count
	, uint32_t* sent_size
	, uint32_t flags
	, WSAOVERLAPPED* wsa_overlapped
	, LPWSAOVERLAPPED_COMPLETION_ROUTINE wsa_completion_routine
)
{
	TRACE_FX();
	if (flags) {
		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
			, "%s flags (0x%08x) must be 0 on perpetual_socket (0x%zx)."
			, __func__
			, flags
			, perpetual_socket
		);
		
		WSASetLastError(WSAEOPNOTSUPP);
		return SOCKET_ERROR;
	}
	if (wsa_completion_routine) {
		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
			, "%s wsa_completion_routine must be NULL on perpetual_socket (0x%zx)."
			, __func__
			, perpetual_socket
		);
		
		WSASetLastError(WSAEOPNOTSUPP);
		return SOCKET_ERROR;
	}
	
	while (1) {
		SOCKET transitorySocket = XSocketGetTransitorySocket(perpetual_socket);
		if (transitorySocket == INVALID_SOCKET) {
			WSASetLastError(WSAENOTSOCK);
			return SOCKET_ERROR;
		}
		
		int32_t resultSend = WSASend(
			transitorySocket
			, wsa_buffers
			, wsa_buffer_count
			, (DWORD*)sent_size
			, flags
			, wsa_overlapped
			, wsa_completion_routine
		);
		int errorSend = WSAGetLastError();
		
		if (resultSend == SOCKET_ERROR && errorSend != WSA_IO_PENDING && XSocketPerpetualSocketChangedError(errorSend, perpetual_socket, transitorySocket)) {
			continue;
		}
		
		if (resultSend == SOCKET_ERROR && errorSend != WSA_IO_PENDING) {
			XLLN_DEBUG_LOG_ECODE(errorSend, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
				, "%s send error on socket (P,T) (0x%zx,0x%zx)."
				, __func__
				, perpetual_socket
				, transitorySocket
			);
		}
		
		WSASetLastError(errorSend);
		return resultSend;
	}
}

// #25
int32_t WINAPI XWSASendTo(
	SOCKET perpetual_socket
	// each buffer is essentially its own message afaik. so wrap each one.
	, WSABUF* wsa_buffers
	, uint32_t wsa_buffer_count
	, uint32_t* sent_size
	, uint32_t flags
	, const sockaddr* address_to
	, int32_t address_to_size
	, WSAOVERLAPPED* wsa_overlapped
	, LPWSAOVERLAPPED_COMPLETION_ROUTINE wsa_completion_routine
)
{
	TRACE_FX();
	if (flags) {
		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
			, "%s flags (0x%08x) must be 0 on perpetual_socket (0x%zx)."
			, __func__
			, flags
			, perpetual_socket
		);
		
		WSASetLastError(WSAEOPNOTSUPP);
		return SOCKET_ERROR;
	}
	if (wsa_completion_routine) {
		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
			, "%s wsa_completion_routine must be NULL on perpetual_socket (0x%zx)."
			, __func__
			, perpetual_socket
		);
		
		WSASetLastError(WSAEOPNOTSUPP);
		return SOCKET_ERROR;
	}
	
	WSA_OVERLAPPED_SOCKET_INFO* wsaOverlappedInfo = 0;
	{
		EnterCriticalSection(&xlive_critsec_overlapped_sockets);
		
		if (xlive_overlapped_sockets_send.count(perpetual_socket)) {
			wsaOverlappedInfo = xlive_overlapped_sockets_send[perpetual_socket];
		}
		else {
			wsaOverlappedInfo = new WSA_OVERLAPPED_SOCKET_INFO;
			wsaOverlappedInfo->perpetualSocket = perpetual_socket;
			wsaOverlappedInfo->isRecv = false;
			xlive_overlapped_sockets_send[perpetual_socket] = wsaOverlappedInfo;
			
			wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersInternalCount = 1;
			wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersInternal = new XWSA_BUFFER[wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersInternalCount];
			for (uint32_t iBuf = 0; iBuf < wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersInternalCount; iBuf++) {
				wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersInternal[iBuf].dataBufferSize = 1024;
				wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersInternal[iBuf].dataBuffer = new uint8_t[wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersInternal[iBuf].dataBufferSize];
			}
			wsaOverlappedInfo->hasCompleted = true;
		}
		
		if (!wsaOverlappedInfo->hasCompleted) {
			LeaveCriticalSection(&xlive_critsec_overlapped_sockets);
			
			XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
				, "%s an existing Overlapped Send operation is already in progress on perpetual_socket (0x%zx)."
				, __func__
				, perpetual_socket
			);
			
			WSASetLastError(WSAEINPROGRESS);
			return SOCKET_ERROR;
		}
		
		wsaOverlappedInfo->hasCompleted = false;
		wsaOverlappedInfo->wsaOverlapped = wsa_overlapped;
		wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersCount = wsa_buffer_count;
		wsaOverlappedInfo->dataBufferSizeResult = 0;
		wsaOverlappedInfo->dataTransferred = 0;
		wsaOverlappedInfo->socketFlagReturned = 0;
		
		LeaveCriticalSection(&xlive_critsec_overlapped_sockets);
	}
	
	for (uint32_t iBuf = 0; iBuf < wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersCount; iBuf++) {
		wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersInternal[iBuf].dataSize = 0;
	}
	
	wsaOverlappedInfo->dataTransferred = 0;
	for (uint32_t iBuf = 0; iBuf < wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersCount; iBuf++) {
		wsaOverlappedInfo->dataTransferred += wsa_buffers[iBuf].len;
	}
	
	while (1) {
		bool connectionlessSocket = false;
		SOCKET transitorySocket = XSocketGetTransitorySocket(perpetual_socket, &connectionlessSocket);
		if (transitorySocket == INVALID_SOCKET) {
			wsaOverlappedInfo->hasCompleted = true;
			WSASetLastError(WSAENOTSOCK);
			return SOCKET_ERROR;
		}
		
		if (connectionlessSocket && address_to && address_to_size) {
			break;
		}
		
		int32_t resultSendTo = WSASendTo(
			transitorySocket
			, wsa_buffers
			, wsa_buffer_count
			, (DWORD*)&(wsaOverlappedInfo->dataBufferSizeResult)
			, flags
			, address_to
			, address_to_size
			, wsa_overlapped
			, wsa_completion_routine
		);
		int errorSendTo = WSAGetLastError();
		
		if (resultSendTo == SOCKET_ERROR && errorSendTo != WSA_IO_PENDING && XSocketPerpetualSocketChangedError(errorSendTo, perpetual_socket, transitorySocket)) {
			continue;
		}
		
		if (resultSendTo != ERROR_SUCCESS && errorSendTo != WSA_IO_PENDING) {
			XLLN_DEBUG_LOG_ECODE(errorSendTo, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
				, "%s sendto error on socket (P,T) (0x%zx,0x%zx)."
				, __func__
				, perpetual_socket
				, transitorySocket
			);
			
			wsaOverlappedInfo->hasCompleted = true;
			WSASetLastError(errorSendTo);
			return resultSendTo;
		}
		
		if (resultSendTo == ERROR_SUCCESS) {
			wsaOverlappedInfo->hasCompleted = true;
			
			XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_DEBUG
				, "%s socket (P,T) (0x%zx,0x%zx) data to send size 0x%x sent wrapped size 0x%x."
				, __func__
				, perpetual_socket
				, transitorySocket
				, wsaOverlappedInfo->dataTransferred
				, wsaOverlappedInfo->dataBufferSizeResult
			);
		}
		
		WSASetLastError(errorSendTo);
		return resultSendTo;
	}
	
	// Connectionless socket with destination from here on. Packet(s) must be wrapped.
	
	if (wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersInternalCount < wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersCount) {
		XWSA_BUFFER* dataBuffersInternalOld = wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersInternal;
		wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersInternal = new XWSA_BUFFER[wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersCount];
		
		for (uint32_t iBuf = 0; iBuf < wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersCount; iBuf++) {
			if (iBuf < wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersInternalCount) {
				wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersInternal[iBuf].dataBufferSize = dataBuffersInternalOld[iBuf].dataBufferSize;
				wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersInternal[iBuf].dataBuffer = dataBuffersInternalOld[iBuf].dataBuffer;
			}
			else {
				wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersInternal[iBuf].dataBufferSize = 1024;
				wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersInternal[iBuf].dataBuffer = new uint8_t[wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersInternal[iBuf].dataBufferSize];
			}
		}
		
		wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersInternalCount = wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersCount;
		delete[] dataBuffersInternalOld;
	}
	
	const int32_t packetSizeHeaderType = sizeof(XLLNNetPacketType::TYPE);
	
	SOCKADDR_STORAGE sockAddrExternal;
	int32_t sockAddrExternalSize = sizeof(sockAddrExternal);
	memcpy(&sockAddrExternal, address_to, sockAddrExternalSize < address_to_size ? sockAddrExternalSize : address_to_size);
	
	const uint32_t ipv4NBO = ((sockaddr_in*)&sockAddrExternal)->sin_addr.s_addr;
	// This address may (hopefully) be an instanceId.
	const uint32_t ipv4HBO = ntohl(ipv4NBO);
	const uint16_t portHBO = GetSockAddrPort(&sockAddrExternal);
	
	bool isBroadcast = (((SOCKADDR_STORAGE*)address_to)->ss_family == AF_INET && (ipv4HBO == INADDR_BROADCAST || ipv4HBO == INADDR_ANY));
	
	for (uint32_t iBuf = 0; iBuf < wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersCount; iBuf++) {
		uint32_t bufferSizeRequired = packetSizeHeaderType + wsa_buffers[iBuf].len;
		if (bufferSizeRequired > wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersInternal[iBuf].dataBufferSize) {
			delete[] wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersInternal[iBuf].dataBuffer;
			wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersInternal[iBuf].dataBufferSize = bufferSizeRequired;
			wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersInternal[iBuf].dataBuffer = new uint8_t[wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersInternal[iBuf].dataBufferSize];
		}
		
		uint8_t* newPacketBufferTitleData = wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersInternal[iBuf].dataBuffer;
		int newPacketBufferTitleDataAlreadyProcessedOffset = 0;
		
		XLLNNetPacketType::TYPE &newPacketTitleDataType = *(XLLNNetPacketType::TYPE*)&newPacketBufferTitleData[newPacketBufferTitleDataAlreadyProcessedOffset];
		newPacketBufferTitleDataAlreadyProcessedOffset += packetSizeHeaderType;
		newPacketTitleDataType = isBroadcast ? XLLNNetPacketType::tTITLE_BROADCAST_PACKET : XLLNNetPacketType::tTITLE_PACKET;
		
		memcpy(&newPacketBufferTitleData[newPacketBufferTitleDataAlreadyProcessedOffset], wsa_buffers[iBuf].buf, wsa_buffers[iBuf].len);
		newPacketBufferTitleDataAlreadyProcessedOffset += wsa_buffers[iBuf].len;
		
		wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersInternal[iBuf].dataSize = newPacketBufferTitleDataAlreadyProcessedOffset;
	}
	
	if (isBroadcast) {
		for (uint32_t iBuf = 0; iBuf < wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersCount; iBuf++) {
			int32_t resultSend = SOCKET_ERROR;
			int32_t errorSend = WSAEINVAL;
			if (
				XllnSocketBroadcastTo(
					&resultSend
					, perpetual_socket
					, (char*)wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersInternal[iBuf].dataBuffer
					, wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersInternal[iBuf].dataSize
					, flags
					, address_to
					, address_to_size
				)
			) {
				if (resultSend != ERROR_SUCCESS) {
					errorSend = WSAGetLastError();
					
					XLLN_DEBUG_LOG_ECODE(errorSend, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
						, "%s XllnSocketBroadcastTo error on perpetual_socket (0x%zx) with buffer index 0x%x of data size 0x%x."
						, __func__
						, perpetual_socket
						, iBuf
						, wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersInternal[iBuf].dataSize
					);
				}
			}
			else {
				XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
					, "%s perpetual_socket (0x%zx) unexpected failed to broadcast buffer index 0x%x of data size 0x%x."
					, __func__
					, perpetual_socket
					, iBuf
					, wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersInternal[iBuf].dataSize
				);
			}
		}
		
		if (wsaOverlappedInfo->wsaOverlapped) {
			wsaOverlappedInfo->wsaOverlapped->Internal = ERROR_SUCCESS;
			wsaOverlappedInfo->wsaOverlapped->InternalHigh = ERROR_SUCCESS;
			wsaOverlappedInfo->wsaOverlapped->Offset = ERROR_SUCCESS;
			wsaOverlappedInfo->wsaOverlapped->OffsetHigh = ERROR_SUCCESS;
			wsaOverlappedInfo->hasCompleted = true;
			if (wsaOverlappedInfo->wsaOverlapped->hEvent && wsaOverlappedInfo->wsaOverlapped->hEvent != INVALID_HANDLE_VALUE) {
				SetEvent(wsaOverlappedInfo->wsaOverlapped->hEvent);
			}
			WSASetLastError(WSA_IO_PENDING);
			return SOCKET_ERROR;
		}
		
		*sent_size = wsaOverlappedInfo->dataTransferred;
		
		wsaOverlappedInfo->hasCompleted = true;
		WSASetLastError(ERROR_SUCCESS);
		return ERROR_SUCCESS;
	}
	
	uint32_t resultNetter = NetterEntityGetAddrByInstanceIdPort(&sockAddrExternal, ipv4HBO, portHBO);
	if (resultNetter) {
		XLLN_DEBUG_LOG_ECODE(resultNetter, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
			, "%s perpetual_socket (0x%zx) NetterEntityGetAddrByInstanceIdPort failed to find address 0x%08x:%hu."
			, __func__
			, perpetual_socket
			, ipv4HBO
			, portHBO
		);
		
		if (resultNetter == ERROR_PORT_NOT_SET) {
			if (sockAddrExternal.ss_family == AF_INET || sockAddrExternal.ss_family == AF_INET6) {
				SendUnknownUserPacket(
					perpetual_socket
					, 0
					, 0
					, XLLNNetPacketType::tUNKNOWN
					, true
					, &sockAddrExternal
					, false
					, 0
					, ipv4HBO
				);
			}
		}
		
		if (wsaOverlappedInfo->wsaOverlapped) {
			wsaOverlappedInfo->wsaOverlapped->Internal = ERROR_SUCCESS;
			wsaOverlappedInfo->wsaOverlapped->InternalHigh = ERROR_SUCCESS;
			wsaOverlappedInfo->wsaOverlapped->Offset = ERROR_SUCCESS;
			wsaOverlappedInfo->wsaOverlapped->OffsetHigh = ERROR_SUCCESS;
			wsaOverlappedInfo->hasCompleted = true;
			if (wsaOverlappedInfo->wsaOverlapped->hEvent && wsaOverlappedInfo->wsaOverlapped->hEvent != INVALID_HANDLE_VALUE) {
				SetEvent(wsaOverlappedInfo->wsaOverlapped->hEvent);
			}
			WSASetLastError(WSA_IO_PENDING);
			return SOCKET_ERROR;
		}
		
		*sent_size = wsaOverlappedInfo->dataTransferred;
		
		wsaOverlappedInfo->hasCompleted = true;
		WSASetLastError(ERROR_SUCCESS);
		return ERROR_SUCCESS;
	}
	
	{
		char* sockAddrInfo = GET_SOCKADDR_INFO(&sockAddrExternal);
		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_DEBUG
			, "%s perpetual_socket (0x%zx) NetterEntityGetAddrByInstanceIdPort found address/instanceId 0x%08x:%hu as %s."
			, __func__
			, perpetual_socket
			, ipv4HBO
			, portHBO
			, sockAddrInfo ? sockAddrInfo : ""
		);
		if (sockAddrInfo) {
			free(sockAddrInfo);
		}
	}
	
	while (1) {
		SOCKET transitorySocket = XSocketGetTransitorySocket(perpetual_socket);
		if (transitorySocket == INVALID_SOCKET) {
			wsaOverlappedInfo->hasCompleted = true;
			WSASetLastError(WSAENOTSOCK);
			return SOCKET_ERROR;
		}
		
		WSABUF* wsaBufTemp = new WSABUF[wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersCount];
		for (uint32_t iBuf = 0; iBuf < wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersCount; iBuf++) {
			wsaBufTemp[iBuf].buf = (char*)wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersInternal[iBuf].dataBuffer;
			wsaBufTemp[iBuf].len = wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersInternal[iBuf].dataSize;
		}
		
		int32_t resultSendTo = WSASendTo(
			transitorySocket
			, wsaBufTemp
			, wsaOverlappedInfo->SEND_RECV.SEND.dataBuffersCount
			, (DWORD*)&(wsaOverlappedInfo->dataBufferSizeResult)
			, flags
			, (const sockaddr*)&sockAddrExternal
			, sockAddrExternalSize
			, wsa_overlapped
			, wsa_completion_routine
		);
		int32_t errorSendTo = WSAGetLastError();
		
		delete[] wsaBufTemp;
		
		if (resultSendTo == SOCKET_ERROR && errorSendTo != WSA_IO_PENDING && XSocketPerpetualSocketChangedError(errorSendTo, perpetual_socket, transitorySocket)) {
			continue;
		}
		
		if (resultSendTo != ERROR_SUCCESS && errorSendTo != WSA_IO_PENDING) {
			XLLN_DEBUG_LOG_ECODE(errorSendTo, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
				, "%s sendto error on socket (P,T) (0x%zx,0x%zx)."
				, __func__
				, perpetual_socket
				, transitorySocket
			);
			
			wsaOverlappedInfo->hasCompleted = true;
			WSASetLastError(errorSendTo);
			return resultSendTo;
		}
		
		if (resultSendTo == ERROR_SUCCESS) {
			wsaOverlappedInfo->hasCompleted = true;
			
			XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_DEBUG
				, "%s socket (P,T) (0x%zx,0x%zx) data to send size 0x%x sent wrapped size 0x%x."
				, __func__
				, perpetual_socket
				, transitorySocket
				, wsaOverlappedInfo->dataTransferred
				, wsaOverlappedInfo->dataBufferSizeResult
			);
		}
		
		*sent_size = wsaOverlappedInfo->dataTransferred;
		
		WSASetLastError(errorSendTo);
		return resultSendTo;
	}
}

// #28
void WINAPI XWSASetLastError(int32_t error_code)
{
	TRACE_FX();
	WSASetLastError(error_code);
}

// #29
WSAEVENT WINAPI XWSACreateEvent()
{
	TRACE_FX();
	return WSACreateEvent();
	//return CreateEvent(NULL, TRUE, FALSE, NULL);
}

// #30
BOOL WINAPI XWSACloseEvent(WSAEVENT wsa_event)
{
	TRACE_FX();
	return WSACloseEvent(wsa_event);
}

// #31
BOOL WINAPI XWSASetEvent(WSAEVENT wsa_event)
{
	TRACE_FX();
	return WSASetEvent(wsa_event);
}

// #32
BOOL WINAPI XWSAResetEvent(WSAEVENT wsa_event)
{
	TRACE_FX();
	return WSAResetEvent(wsa_event);
}

// #33
uint32_t WINAPI XWSAWaitForMultipleEvents(uint32_t event_count, const WSAEVENT* wsa_events, BOOL wait_on_all, uint32_t timeout, BOOL alertable)
{
	TRACE_FX();
	return WSAWaitForMultipleEvents(event_count, wsa_events, wait_on_all, timeout, alertable);
}

// #34
int32_t WINAPI XWSAFDIsSet(SOCKET perpetual_socket, fd_set* perpetual_set)
{
	TRACE_FX();
	if (!perpetual_set) {
		XLLN_DEBUG_LOG(XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
			, "%s perpetual_set is NULL."
			, __func__
		);
		WSASetLastError(WSAEFAULT);
		return 0;
	}
	
	bool containsSocket = false;
	
	for (u_int iSocket = 0; iSocket < perpetual_set->fd_count; iSocket++) {
		SOCKET transitorySocket = XSocketGetTransitorySocket(perpetual_set->fd_array[iSocket]);
		if (transitorySocket == INVALID_SOCKET) {
			WSASetLastError(WSAENOTSOCK);
			return 0;
		}
		
		if (perpetual_socket == perpetual_set->fd_array[iSocket]) {
			containsSocket = true;
		}
	}
	
	WSASetLastError(ERROR_SUCCESS);
	return containsSocket ? 1 : 0;
}

// #35
int32_t WINAPI XWSAEventSelect(SOCKET perpetual_socket, WSAEVENT wsa_event, uint32_t network_event_flags)
{
	TRACE_FX();
	
	while (1) {
		SOCKET transitorySocket = XSocketGetTransitorySocket(perpetual_socket);
		if (transitorySocket == INVALID_SOCKET) {
			WSASetLastError(WSAENOTSOCK);
			return SOCKET_ERROR;
		}
		
		int32_t resultEventSelect = WSAEventSelect(transitorySocket, wsa_event, network_event_flags);
		int32_t errorEventSelect = WSAGetLastError();
		
		if (resultEventSelect && XSocketPerpetualSocketChangedError(errorEventSelect, perpetual_socket, transitorySocket)) {
			continue;
		}
		
		if (resultEventSelect) {
			XLLN_DEBUG_LOG_ECODE(errorEventSelect, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
				, "%s EventSelect error on socket (P,T) (0x%zx,0x%zx)."
				, __func__
				, perpetual_socket
				, transitorySocket
			);
		}
		
		WSASetLastError(errorEventSelect);
		return resultEventSelect;
	}
}
