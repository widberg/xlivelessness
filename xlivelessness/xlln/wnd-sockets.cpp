#include <winsock2.h>
#include <Windows.h>
#include "../xlive/xdefs.hpp"
#include "./wnd-sockets.hpp"
#include "./xlln.hpp"
#include "../resource.h"
#include "../utils/utils.hpp"
#include "../xlive/packet-handler.hpp"
#include "../xlive/xsocket.hpp"
#include "../xlive/xrender.hpp"
#include "./debug-log.hpp"
#include <stdint.h>
#include <time.h>
#include <CommCtrl.h>

HWND xlln_hwnd_sockets = 0;
static HWND hwndListView = 0;
static HANDLE xlln_window_create_event = INVALID_HANDLE_VALUE;
static HANDLE xlln_window_initialised_event = INVALID_HANDLE_VALUE;
static HANDLE xlln_window_destroy_event = INVALID_HANDLE_VALUE;

static int CreateColumn(HWND hwndLV, int iCol, const wchar_t* text, int iWidth)
{
	LVCOLUMN lvc;
	
	lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
	lvc.fmt = LVCFMT_LEFT;
	lvc.cx = iWidth;
	lvc.pszText = (wchar_t*)text;
	lvc.iSubItem = iCol;
	return ListView_InsertColumn(hwndLV, iCol, &lvc);
}

static int CreateItem(HWND hwndListView, int iItem)
{
	LV_ITEM item;
	item.mask = LVIF_TEXT;
	item.iItem = iItem;
	item.iIndent = 0;
	item.iSubItem = 0;
	item.state = 0;
	item.cColumns = 0;
	item.pszText = (wchar_t*)L"";
	return ListView_InsertItem(hwndListView, &item);
	/*{
		LV_ITEM item;
		item.mask = LVIF_TEXT;
		item.iItem = 0;
		item.iIndent = 0;
		item.iSubItem = 1;
		item.state = 0;
		item.cColumns = 0;
		item.pszText = (wchar_t*)L"Toothbrush";
		ListView_SetItem(hwndList, &item);
	}*/
}

void XllnWndSocketsInvalidateSockets()
{
	InvalidateRect(xlln_hwnd_sockets, NULL, FALSE);
}

static LRESULT CALLBACK DLLWindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message) {
		case WM_SIZE: {
			uint32_t width = LOWORD(lParam);
			uint32_t height = HIWORD(lParam);
			
			HWND controlSocketsList = GetDlgItem(xlln_hwnd_sockets, XLLNControlsMessageNumbers::SOCKETS_LST_SOCKETS);
			MoveWindow(controlSocketsList, 10, 30, width - 20, height - 40, TRUE);
			
			break;
		}
		case WM_PAINT: {
			PAINTSTRUCT ps;
			HDC hdc = BeginPaint(xlln_hwnd_sockets, &ps);
			SetTextColor(hdc, RGB(0, 0, 0));
			SetBkColor(hdc, 0x00C8C8C8);
			
			HBRUSH hBrush = CreateSolidBrush(0x00C8C8C8);
			SelectObject(hdc, hBrush);
			RECT bgRect;
			GetClientRect(hWnd, &bgRect);
			HRGN bgRgn = CreateRectRgnIndirect(&bgRect);
			FillRgn(hdc, bgRgn, hBrush);
			DeleteObject(bgRgn);
			DeleteObject(hBrush);
			
			if (xlive_netsocket_abort) {
				SetTextColor(hdc, RGB(0xFF, 0, 0));
				const char* labelToUse = "NET SOCKET ABORT.";
				TextOutA(hdc, 10, 10, labelToUse, (uint32_t)strlen(labelToUse));
			}
			else {
				SetTextColor(hdc, RGB(0, 0xCA, 0));
				const char* labelToUse = "Sockets Enabled.";
				TextOutA(hdc, 10, 10, labelToUse, (uint32_t)strlen(labelToUse));
			}
			
			SetTextColor(hdc, RGB(0, 0, 0));
			
			{
				char* textLabel = IsUsingBasePort(xlive_base_port)
					? FormMallocString(
						"Base Port: %hd."
						, xlive_base_port
					)
					: 0;
				const char* labelToUse = textLabel ? textLabel : "Base Port: Not in use.";
				TextOutA(hdc, 170, 10, labelToUse, (uint32_t)strlen(labelToUse));
				if (textLabel) {
					free(textLabel);
				}
			}
			
			{
				EnterCriticalSection(&xlive_critsec_sockets);
				char* textLabel = xlive_xsocket_perpetual_core_socket == INVALID_SOCKET
					? 0
					: FormMallocString(
						"Core Socket (P,T): (0x%zx,0x%zx)."
						, xlive_xsocket_perpetual_core_socket
						, xlive_xsocket_perpetual_to_transitory_socket[xlive_xsocket_perpetual_core_socket]
					);
				LeaveCriticalSection(&xlive_critsec_sockets);
				const char* labelToUse = textLabel ? textLabel : "Core Socket: INVALID_SOCKET.";
				TextOutA(hdc, 340, 10, labelToUse, (uint32_t)strlen(labelToUse));
				if (textLabel) {
					free(textLabel);
				}
			}
			
			EndPaint(xlln_hwnd_sockets, &ps);
			
			ListView_DeleteAllItems(hwndListView);
			{
				EnterCriticalSection(&xlive_critsec_sockets);
				
				std::vector<SOCKET_MAPPING_INFO*> socketsOrderedByPort;
				for (auto const &socketInfo : xlive_socket_info) {
					size_t j = 0;
					for (; j < socketsOrderedByPort.size(); j++) {
						SOCKET_MAPPING_INFO* nextSocketInfo = socketsOrderedByPort.at(j);
						if (nextSocketInfo->portOgHBO > socketInfo.second->portOgHBO) {
							socketsOrderedByPort.insert(socketsOrderedByPort.begin() + j, socketInfo.second);
							break;
						}
						else if (nextSocketInfo->portOgHBO == 0 && socketInfo.second->portOgHBO == 0 && nextSocketInfo->portBindHBO > socketInfo.second->portBindHBO) {
							socketsOrderedByPort.insert(socketsOrderedByPort.begin() + j, socketInfo.second);
							break;
						}
					}
					if (j == socketsOrderedByPort.size()) {
						socketsOrderedByPort.push_back(socketInfo.second);
					}
				}
				
				for (uint32_t i = 0; i < socketsOrderedByPort.size(); i++) {
					SOCKET_MAPPING_INFO* socketInfo = socketsOrderedByPort[i];
					
					wchar_t* textItemLabel;
					uint32_t j = 0;
					CreateItem(hwndListView, i);
#define AddSocketItemColumn(format, ...) textItemLabel = FormMallocString(format, __VA_ARGS__); ListView_SetItemText(hwndListView, i, j++, textItemLabel); free(textItemLabel)
					
					AddSocketItemColumn(L"0x%zx", socketInfo->perpetualSocket);
					AddSocketItemColumn(L"0x%zx", socketInfo->transitorySocket);
					AddSocketItemColumn(L"%d", socketInfo->type);
					AddSocketItemColumn(L"%hs(%d)"
						, socketInfo->protocol == IPPROTO_UDP ? "UDP" : (socketInfo->protocol == IPPROTO_TCP ? "TCP" : "")
						, socketInfo->protocol
					);
					AddSocketItemColumn(L"%hs", socketInfo->isVdpProtocol ? "Y" : "");
					AddSocketItemColumn(L"%hs", socketInfo->broadcast ? "Y" : "");
					AddSocketItemColumn(L"%hu", socketInfo->portOgHBO);
					AddSocketItemColumn(L"%hd", socketInfo->portOffsetHBO);
					AddSocketItemColumn(L"%hu", socketInfo->portBindHBO);
					
#undef AddSocketItemColumn
				}
				
				LeaveCriticalSection(&xlive_critsec_sockets);
			}
			
			break;
		}
		case WM_SYSCOMMAND: {
			if (wParam == SC_CLOSE) {
				XllnShowWindow(XllnShowType::SOCKETS_HIDE);
				return 0;
			}
			break;
		}
		case WM_NOTIFY: {
			switch (LOWORD(wParam)) {
				case XLLNControlsMessageNumbers::SOCKETS_LST_SOCKETS: {
					
					NMHDR* nmhdr = (NMHDR*)lParam;
					//if (nmhdr->code == NM_DBLCLK) {}
					//else if (nmhdr->code == NM_RETURN) {}
					
					break;
				}
			}
			break;
		}
		case WM_CTLCOLORSTATIC: {
			HDC hdc = reinterpret_cast<HDC>(wParam);
			SetTextColor(hdc, RGB(0, 0, 0));
			SetBkColor(hdc, 0x00C8C8C8);
			return (INT_PTR)CreateSolidBrush(0x00C8C8C8);
		}
		case WM_CREATE: {
			hwndListView = CreateWindowA(WC_LISTVIEWA, "",
				WS_VISIBLE | WS_BORDER | WS_CHILD | LVS_REPORT | LVS_NOSORTHEADER | WS_TABSTOP,
				10, 30, 640, 110,
				hWnd, (HMENU)XLLNControlsMessageNumbers::SOCKETS_LST_SOCKETS, xlln_hModule, 0);
			
			uint32_t j = 0;
			CreateColumn(hwndListView, ++j, L"Perpetual Socket", 105);
			CreateColumn(hwndListView, ++j, L"Transitory Socket", 110);
			CreateColumn(hwndListView, ++j, L"Type", 40);
			CreateColumn(hwndListView, ++j, L"Protocol", 60);
			CreateColumn(hwndListView, ++j, L"was VDP", 60);
			CreateColumn(hwndListView, ++j, L"Broadcast", 70);
			CreateColumn(hwndListView, ++j, L"Port", 50);
			CreateColumn(hwndListView, ++j, L"Port Offset", 70);
			CreateColumn(hwndListView, ++j, L"Bind Port", 65);
			
			SetEvent(xlln_window_create_event);
			
			break;
		}
		case WM_DESTROY: {
			PostQuitMessage(0);
			return 0;
		}
	}
	
	return DefWindowProcW(hWnd, message, wParam, lParam);
}

static DWORD WINAPI XllnThreadWndSockets(void* lpParam)
{
	srand((unsigned int)time(NULL));
	
	xlln_window_create_event = CreateEventA(NULL, FALSE, FALSE, NULL);
	
	const wchar_t* windowclassname = L"XLLNDLLWindowSocketsClass";
	HINSTANCE hModule = reinterpret_cast<HINSTANCE>(lpParam);
	
	WNDCLASSEXW wc;
	wc.hInstance = hModule;
	wc.lpszClassName = windowclassname;
	wc.lpfnWndProc = DLLWindowProc;
	wc.style = CS_DBLCLKS;
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.hIcon = LoadIconW(xlln_hModule, XLLN_LOGO_COMPACT_SQUARE_ICON);
	wc.hIconSm = LoadIconW(xlln_hModule, XLLN_LOGO_COMPACT_SQUARE_ICON);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.lpszMenuName = NULL;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hbrBackground = (HBRUSH)COLOR_BACKGROUND;
	if (!RegisterClassExW(&wc)) {
		return FALSE;
	}
	
	{
		wchar_t* windowTitle = FormMallocString(L"XLLN Sockets #%d v%d.%d.%d.%d", xlln_local_instance_index, DLL_VERSION);
		
		HWND hwdParent = NULL;
		xlln_hwnd_sockets = CreateWindowExW(0, windowclassname, windowTitle, WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MINIMIZEBOX, CW_USEDEFAULT, CW_USEDEFAULT, 675, 200, hwdParent, 0, hModule, NULL);
		
		free(windowTitle);
		windowTitle = 0;
	}
	
	DWORD resultWait = WaitForSingleObject(xlln_window_create_event, INFINITE);
	if (resultWait != WAIT_OBJECT_0) {
		XLLN_DEBUG_LOG_ECODE(resultWait, XLLN_LOG_CONTEXT_XLIVELESSNESS | XLLN_LOG_LEVEL_FATAL
			, "%s failed to wait for the window to be created."
			, __func__
		);
	}
	CloseHandle(xlln_window_create_event);
	xlln_window_create_event = INVALID_HANDLE_VALUE;
	
	ShowWindow(xlln_hwnd_sockets, SW_HIDE);
	
	SetEvent(xlln_window_initialised_event);
	
	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0)) {
		// Do message processing here.
		
		// Handle tab ordering.
		bool handleListViewReturnKey = (msg.hwnd == hwndListView && LOWORD(msg.message) == WM_KEYDOWN && msg.wParam == VK_RETURN);
		if (handleListViewReturnKey || !IsDialogMessage(xlln_hwnd_sockets, &msg)) {
			// Translate virtual-key msg into character msg
			TranslateMessage(&msg);
			// Send msg to WindowProcedure(s)
			DispatchMessage(&msg);
		}
	}
	
	SetEvent(xlln_window_destroy_event);
	
	return ERROR_SUCCESS;
}

uint32_t InitXllnWndSockets()
{
	xlln_window_destroy_event = CreateEventA(NULL, FALSE, FALSE, NULL);
	xlln_window_initialised_event = CreateEventA(NULL, FALSE, FALSE, NULL);
	
	CreateThread(0, NULL, XllnThreadWndSockets, (void*)xlln_hModule, NULL, NULL);
	
	DWORD resultWait = WaitForSingleObject(xlln_window_initialised_event, INFINITE);
	if (resultWait != WAIT_OBJECT_0) {
		XLLN_DEBUG_LOG_ECODE(resultWait, XLLN_LOG_CONTEXT_XLIVELESSNESS | XLLN_LOG_LEVEL_FATAL
			, "%s failed to wait for the window to finish initialising."
			, __func__
		);
	}
	CloseHandle(xlln_window_initialised_event);
	xlln_window_initialised_event = INVALID_HANDLE_VALUE;
	
	return ERROR_SUCCESS;
}

uint32_t UninitXllnWndSockets()
{
	SendMessage(xlln_hwnd_sockets, WM_CLOSE, (WPARAM)0, (LPARAM)0);
	
	DWORD resultWait = WaitForSingleObject(xlln_window_destroy_event, INFINITE);
	if (resultWait != WAIT_OBJECT_0) {
		XLLN_DEBUG_LOG_ECODE(resultWait, XLLN_LOG_CONTEXT_XLIVELESSNESS | XLLN_LOG_LEVEL_FATAL
			, "%s failed to wait for the window to be destroyed."
			, __func__
		);
	}
	CloseHandle(xlln_window_destroy_event);
	xlln_window_destroy_event = INVALID_HANDLE_VALUE;
	
	return ERROR_SUCCESS;
}
