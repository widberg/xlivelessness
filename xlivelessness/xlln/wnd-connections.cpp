#include <winsock2.h>
#include <Windows.h>
#include "../xlive/xdefs.hpp"
#include "../xlive/xnet.hpp"
#include "./wnd-connections.hpp"
#include "./debug-log.hpp"
#include "./xlln.hpp"
#include "../resource.h"
#include "../utils/utils.hpp"
#include "../utils/util-socket.hpp"
#include "../xlive/xsocket.hpp"
#include "../xlive/net-entity.hpp"
#include "../xlive/xrender.hpp"
#include <stdint.h>
#include <time.h>
#include <CommCtrl.h>

HWND xlln_hwnd_connections = 0;
static HWND hwndTreeView = 0;
static HANDLE xlln_window_create_event = INVALID_HANDLE_VALUE;
static HANDLE xlln_window_initialised_event = INVALID_HANDLE_VALUE;
static HANDLE xlln_window_destroy_event = INVALID_HANDLE_VALUE;

void XllnWndConnectionsInvalidateConnections()
{
	InvalidateRect(xlln_hwnd_connections, NULL, FALSE);
}

static HTREEITEM AddItemToTree(HWND hwndTV, LPTSTR lpszItem, int nLevel)
{
	TVITEM tvi;
	TVINSERTSTRUCT tvins;
	static HTREEITEM hPrev = (HTREEITEM)TVI_FIRST;
	static HTREEITEM hPrevRootItem = NULL;
	static HTREEITEM hPrevLev2Item = NULL;
	HTREEITEM hti;

	tvi.mask = TVIF_TEXT | TVIF_IMAGE
		| TVIF_SELECTEDIMAGE | TVIF_PARAM;

	// Set the text of the item. 
	tvi.pszText = lpszItem;
	tvi.cchTextMax = sizeof(tvi.pszText) / sizeof(tvi.pszText[0]);

	tvi.iImage = 0;
	tvi.iSelectedImage = 0;

	// Save the heading level in the item's application-defined 
	// data area. 
	tvi.lParam = (LPARAM)nLevel;
	tvins.item = tvi;
	tvins.hInsertAfter = hPrev;

	// Set the parent item based on the specified level. 
	if (nLevel == 1)
		tvins.hParent = TVI_ROOT;
	else if (nLevel == 2)
		tvins.hParent = hPrevRootItem;
	else
		tvins.hParent = hPrevLev2Item;

	// Add the item to the tree-view control. 
	hPrev = (HTREEITEM)SendMessage(hwndTV, TVM_INSERTITEM,
		0, (LPARAM)(LPTVINSERTSTRUCT)&tvins);

	if (hPrev == NULL)
		return NULL;

	// Save the handle to the item. 
	if (nLevel == 1)
		hPrevRootItem = hPrev;
	else if (nLevel == 2)
		hPrevLev2Item = hPrev;

	// The new item is a child item. Give the parent item a 
	// closed folder bitmap to indicate it now has child items. 
	if (nLevel > 1)
	{
		hti = TreeView_GetParent(hwndTV, hPrev);
		tvi.mask = TVIF_IMAGE | TVIF_SELECTEDIMAGE;
		tvi.hItem = hti;
		tvi.iImage = 0;
		tvi.iSelectedImage = 0;
		TreeView_SetItem(hwndTV, &tvi);
	}

	return hPrev;
}

static LRESULT CALLBACK DLLWindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message) {
		case WM_SIZE: {
			uint32_t width = LOWORD(lParam);
			uint32_t height = HIWORD(lParam);
			
			HWND controlSocketsList = GetDlgItem(xlln_hwnd_connections, XLLNControlsMessageNumbers::CONNS_TRE_CONNECTIONS);
			MoveWindow(controlSocketsList, 10, 40, width - 20, height - 50, TRUE);
			
			break;
		}
		case WM_PAINT: {
			PAINTSTRUCT ps;
			HDC hdc = BeginPaint(xlln_hwnd_connections, &ps);
			SetTextColor(hdc, RGB(0, 0, 0));
			SetBkColor(hdc, 0x00C8C8C8);
			
			HBRUSH hBrush = CreateSolidBrush(0x00C8C8C8);
			SelectObject(hdc, hBrush);
			RECT bgRect;
			GetClientRect(hWnd, &bgRect);
			HRGN bgRgn = CreateRectRgnIndirect(&bgRect);
			FillRgn(hdc, bgRgn, hBrush);
			DeleteObject(bgRgn);
			DeleteObject(hBrush);
			
			{
				char* textLabel = FormMallocString(
					"Local Instance Id: 0x%08x."
					, ntohl(xlive_local_xnAddr.inaOnline.s_addr)
				);
				TextOutA(hdc, 190, 15, textLabel, (uint32_t)strlen(textLabel));
				free(textLabel);
			}
			
			EndPaint(xlln_hwnd_connections, &ps);
			
			TreeView_DeleteAllItems(hwndTreeView);
			
			EnterCriticalSection(&xlln_critsec_net_entity);
			
			std::map<SOCKADDR_STORAGE*, NET_ENTITY*> externalAddrsToNetter = xlln_net_entity_external_addr_to_netentity;
			
			wchar_t* textItemLabel;
#define AddItemToTree2(level, format, ...) textItemLabel = FormMallocString(format, __VA_ARGS__); AddItemToTree(hwndTreeView, (LPTSTR)textItemLabel, level); free(textItemLabel)
			
			for (auto const &instanceNetEntity : xlln_net_entity_instanceid_to_netentity) {
				
				if (IsUsingBasePort(instanceNetEntity.second->portBaseHBO)) {
					AddItemToTree2(1, L"Instance: 0x%08x, BasePort: %hu", instanceNetEntity.second->instanceId, instanceNetEntity.second->portBaseHBO);
				}
				else {
					AddItemToTree2(1, L"Instance: 0x%08x, BasePort: N/A", instanceNetEntity.second->instanceId);
				}
				
				for (auto const &portMap : instanceNetEntity.second->external_addr_to_port_internal) {
					
					auto const &externalAddrToNetter = externalAddrsToNetter.find(portMap.first);
					const wchar_t* label;
					if (externalAddrToNetter != externalAddrsToNetter.end() && externalAddrToNetter->second == instanceNetEntity.second) {
						label = L"---> %hu, %hd --> %hs";
						externalAddrsToNetter.erase(portMap.first);
					}
					else {
						label = L"-/-> %hu, %hd --> %hs";
					}
					
					char* sockAddrInfo = GetSockAddrInfo(portMap.first);
					AddItemToTree2(2, label, portMap.second.first, portMap.second.second, sockAddrInfo);
					free(sockAddrInfo);
				}
			}
			
			for (auto const &externalAddrToNetter : externalAddrsToNetter) {
				AddItemToTree2(1, L"ERROR Remainder: &ExternalAddr: 0x%08x, &Netter: 0x%08x", externalAddrToNetter.first, externalAddrToNetter.second);
			}
			
#undef AddItemToTree2
			
			LeaveCriticalSection(&xlln_critsec_net_entity);
			
			break;
		}
		case WM_SYSCOMMAND: {
			if (wParam == SC_CLOSE) {
				XllnShowWindow(XllnShowType::CONNECTIONS_HIDE);
				return 0;
			}
			break;
		}
		case WM_COMMAND: {
			if (wParam == XLLNControlsMessageNumbers::CONNS_BTN_REFRESH_CONNS) {
				XllnWndConnectionsInvalidateConnections();
			}
			else if (wParam == XLLNControlsMessageNumbers::CONNS_BTN_DELETE_CONNS) {
				NetterEntityClearAllPortMappings();
				XllnWndConnectionsInvalidateConnections();
			}
			
			return 0;
		}
		case WM_CTLCOLORSTATIC: {
			HDC hdc = reinterpret_cast<HDC>(wParam);
			SetTextColor(hdc, RGB(0, 0, 0));
			SetBkColor(hdc, 0x00C8C8C8);
			return (INT_PTR)CreateSolidBrush(0x00C8C8C8);
		}
		case WM_CREATE: {
			CreateWindowA(WC_BUTTONA, "Refresh", WS_CHILD | WS_VISIBLE | WS_TABSTOP,
				10, 10, 75, 25, hWnd, (HMENU)XLLNControlsMessageNumbers::CONNS_BTN_REFRESH_CONNS, xlln_hModule, NULL);
			
			CreateWindowA(WC_BUTTONA, "Clear", WS_CHILD | WS_VISIBLE | WS_TABSTOP,
				100, 10, 75, 25, hWnd, (HMENU)XLLNControlsMessageNumbers::CONNS_BTN_DELETE_CONNS, xlln_hModule, NULL);
			
			hwndTreeView = CreateWindowA(WC_TREEVIEWA, "",
				WS_VISIBLE | WS_BORDER | WS_CHILD | TVS_HASBUTTONS | TVS_HASLINES | TVS_LINESATROOT,
				10, 40, 400, 310,
				hWnd, (HMENU)XLLNControlsMessageNumbers::CONNS_TRE_CONNECTIONS, xlln_hModule, 0);
			
			SetEvent(xlln_window_create_event);
			
			break;
		}
		case WM_DESTROY: {
			PostQuitMessage(0);
			return 0;
		}
	}
	
	return DefWindowProcW(hWnd, message, wParam, lParam);
}

static DWORD WINAPI XllnThreadWndConnections(void* lpParam)
{
	srand((unsigned int)time(NULL));
	
	xlln_window_create_event = CreateEventA(NULL, FALSE, FALSE, NULL);
	
	const wchar_t* windowclassname = L"XLLNDLLWindowConnectionsClass";
	HINSTANCE hModule = reinterpret_cast<HINSTANCE>(lpParam);
	
	WNDCLASSEXW wc;
	wc.hInstance = hModule;
	wc.lpszClassName = windowclassname;
	wc.lpfnWndProc = DLLWindowProc;
	wc.style = CS_DBLCLKS;
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.hIcon = LoadIconW(xlln_hModule, XLLN_LOGO_COMPACT_SQUARE_ICON);
	wc.hIconSm = LoadIconW(xlln_hModule, XLLN_LOGO_COMPACT_SQUARE_ICON);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.lpszMenuName = NULL;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hbrBackground = (HBRUSH)COLOR_BACKGROUND;
	if (!RegisterClassExW(&wc)) {
		return FALSE;
	}
	
	{
		wchar_t* windowTitle = FormMallocString(L"XLLN Connections #%d v%d.%d.%d.%d", xlln_local_instance_index, DLL_VERSION);
		
		HWND hwdParent = NULL;
		xlln_hwnd_connections = CreateWindowExW(0, windowclassname, windowTitle, WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, 440, 400, hwdParent, 0, hModule, NULL);
		
		free(windowTitle);
		windowTitle = 0;
	}
	
	DWORD resultWait = WaitForSingleObject(xlln_window_create_event, INFINITE);
	if (resultWait != WAIT_OBJECT_0) {
		XLLN_DEBUG_LOG_ECODE(resultWait, XLLN_LOG_CONTEXT_XLIVELESSNESS | XLLN_LOG_LEVEL_FATAL
			, "%s failed to wait for the window to be created."
			, __func__
		);
	}
	CloseHandle(xlln_window_create_event);
	xlln_window_create_event = INVALID_HANDLE_VALUE;
	
	ShowWindow(xlln_hwnd_connections, SW_HIDE);
	
	SetEvent(xlln_window_initialised_event);
	
	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0)) {
		// Do message processing here.
		
		// Handle tab ordering
		if (!IsDialogMessage(xlln_hwnd_connections, &msg)) {
			// Translate virtual-key msg into character msg
			TranslateMessage(&msg);
			// Send msg to WindowProcedure(s)
			DispatchMessage(&msg);
		}
	}
	
	SetEvent(xlln_window_destroy_event);
	
	return ERROR_SUCCESS;
}

uint32_t InitXllnWndConnections()
{
	xlln_window_destroy_event = CreateEventA(NULL, FALSE, FALSE, NULL);
	xlln_window_initialised_event = CreateEventA(NULL, FALSE, FALSE, NULL);
	
	CreateThread(0, NULL, XllnThreadWndConnections, (void*)xlln_hModule, NULL, NULL);
	
	DWORD resultWait = WaitForSingleObject(xlln_window_initialised_event, INFINITE);
	if (resultWait != WAIT_OBJECT_0) {
		XLLN_DEBUG_LOG_ECODE(resultWait, XLLN_LOG_CONTEXT_XLIVELESSNESS | XLLN_LOG_LEVEL_FATAL
			, "%s failed to wait for the window to finish initialising."
			, __func__
		);
	}
	CloseHandle(xlln_window_initialised_event);
	xlln_window_initialised_event = INVALID_HANDLE_VALUE;
	
	return ERROR_SUCCESS;
}

uint32_t UninitXllnWndConnections()
{
	SendMessage(xlln_hwnd_connections, WM_CLOSE, (WPARAM)0, (LPARAM)0);
	
	DWORD resultWait = WaitForSingleObject(xlln_window_destroy_event, INFINITE);
	if (resultWait != WAIT_OBJECT_0) {
		XLLN_DEBUG_LOG_ECODE(resultWait, XLLN_LOG_CONTEXT_XLIVELESSNESS | XLLN_LOG_LEVEL_FATAL
			, "%s failed to wait for the window to be destroyed."
			, __func__
		);
	}
	CloseHandle(xlln_window_destroy_event);
	xlln_window_destroy_event = INVALID_HANDLE_VALUE;
	
	return ERROR_SUCCESS;
}
