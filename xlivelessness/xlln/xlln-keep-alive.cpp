#include <winsock2.h>
#include <Windows.h>
#include "./xlln-keep-alive.hpp"
#include "../xlive/xdefs.hpp"
#include "../xlive/xfuncs.hpp"
#include "./debug-log.hpp"
#include "../xlln/wnd-main.hpp"
#include "../xlive/xlive.hpp"
#include "../xlive/packet-handler.hpp"
#include "../xlive/xlocator.hpp"
#include "../xlive/xnet.hpp"
#include "../resource.h"
#include <thread>

static HANDLE xlln_keep_alive_thread_event = INVALID_HANDLE_VALUE;
static bool xlln_keep_alive_thread_shutdown = false;
static std::thread xlln_keep_alive_thread;

static void ThreadKeepAlive()
{
	const size_t hubRequestPacketBufferSize = sizeof(XLLNNetPacketType::TYPE) + sizeof(XLLNNetPacketType::HUB_REQUEST_PACKET);
	uint8_t* hubRequestPacketBuffer = new uint8_t[hubRequestPacketBufferSize];
	hubRequestPacketBuffer[0] = XLLNNetPacketType::tHUB_REQUEST;
	XLLNNetPacketType::HUB_REQUEST_PACKET* hubRequest = (XLLNNetPacketType::HUB_REQUEST_PACKET*)&hubRequestPacketBuffer[sizeof(XLLNNetPacketType::TYPE)];
	hubRequest->xllnVersion = (DLL_VERSION_MAJOR << 24) + (DLL_VERSION_MINOR << 16) + (DLL_VERSION_REVISION << 8) + DLL_VERSION_BUILD;
	hubRequest->titleId = xlive_title_id;
	hubRequest->titleVersion = xlive_title_version;
	
	__time64_t nextHubIteration = 0;
	
	while (1) {
		TRACE_FX();
		
		__time64_t ltime;
		_time64(&ltime);//seconds since epoch.
		
		if (xlln_direct_ip_connect.timeoutAt && xlln_direct_ip_connect.timeoutAt < ltime) {
			XllnDirectIpConnectCancel();
			MessageBoxW(xlln_window_hwnd, L"Connection attempt timed out.", L"XLLN Direct IP Connect Error", MB_OK);
		}
		
		if (nextHubIteration < ltime) {
			// Check again in about 5 seconds.
			nextHubIteration = ltime + 5;
			hubRequest->instanceId = ntohl(xlive_local_xnAddr.inaOnline.s_addr);
			hubRequest->portBaseHBO = xlive_base_port;
			
			if (hubRequest->instanceId) {
				EnterCriticalSection(&xlive_critsec_broadcast_addresses);
				
				__time64_t timeToSendKeepAlive = ltime - 30;
				for (auto const &broadcastEntity : xlive_broadcast_addresses) {
					if (broadcastEntity.entityType != XLLNBroadcastEntity::TYPE::tHUB_SERVER && broadcastEntity.entityType != XLLNBroadcastEntity::TYPE::tUNKNOWN) {
						continue;
					}
					if (broadcastEntity.lastComm > timeToSendKeepAlive) {
						continue;
					}
					
					SendToPerpetualSocket(
						xlive_xsocket_perpetual_core_socket
						, (char*)hubRequestPacketBuffer
						, (int)hubRequestPacketBufferSize
						, 0
						, (const sockaddr*)&broadcastEntity.sockaddr
						, sizeof(broadcastEntity.sockaddr)
					);
				}
				
				LeaveCriticalSection(&xlive_critsec_broadcast_addresses);
			}
		}
		
		DWORD resultWait = WaitForSingleObject(xlln_keep_alive_thread_event, 1000);
		if (resultWait != WAIT_OBJECT_0 && resultWait != STATUS_TIMEOUT) {
			XLLN_DEBUG_LOG_ECODE(resultWait, XLLN_LOG_CONTEXT_XLIVE | XLLN_LOG_LEVEL_ERROR
				, "%s failed to wait on xlln_keep_alive_thread_event (0x%08x)."
				, __func__
				, xlln_keep_alive_thread_event
			);
			break;
		}
		
		if (xlln_keep_alive_thread_shutdown) {
			break;
		}
	}
	
	delete[] hubRequestPacketBuffer;
	hubRequestPacketBuffer = 0;
}

void XllnThreadKeepAliveStart()
{
	TRACE_FX();
	
	XllnThreadKeepAliveStop();
	xlln_keep_alive_thread_event = CreateEventA(NULL, FALSE, FALSE, NULL);
	xlln_keep_alive_thread_shutdown = false;
	xlln_keep_alive_thread = std::thread(ThreadKeepAlive);
}

void XllnThreadKeepAliveStop()
{
	TRACE_FX();
	
	if (xlln_keep_alive_thread_event != INVALID_HANDLE_VALUE) {
		xlln_keep_alive_thread_shutdown = true;
		SetEvent(xlln_keep_alive_thread_event);
		xlln_keep_alive_thread.join();
		CloseHandle(xlln_keep_alive_thread_event);
		xlln_keep_alive_thread_event = INVALID_HANDLE_VALUE;
	}
}
